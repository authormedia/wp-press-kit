=== WP Press Kit by Author Media ===
Contributors: authormedia, zookatron, neovita
Tags: press kit, publicity
Requires at least: 4.5.0
Tested up to: 4.5.3
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A WordPress plugin built specifically to help people make press kits.

== Description ==

This is WP Press Kit.

== Changelog ==

= 0.1.0 =
* Initial Beta Version
