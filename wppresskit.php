<?php
/*
Plugin Name: WP Press Kit (Beta)
Plugin URI: http://www.wppresskit.com/
Description: Create a powerful press kit or media kit quickly and easily
Author: Author Media
Author URI: http://www.authormedia.com
Domain Path: /languages/
Version: 0.1.2
*/

// abort if this file is called directly
if(!defined('WPINC')) { die; }

// ensure PHP version is defined
if(!defined('PHP_VERSION_ID')) {
	$version = explode('.', PHP_VERSION);
	define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

// check PHP version
if(PHP_VERSION_ID < 50600) {
	function wppresskit_php_version_admin_notice() {
		load_plugin_textdomain('wp-press-kit', false, dirname(plugin_basename(__FILE__)).'/languages');
		?>
		<div id="message" class="error">
			<p>
				<strong><?php _e('PHP Out of Date', 'wp-press-kit'); ?></strong> &#8211;
				<?php printf(__('WP Press Kit requires at least PHP 5.6.0. You are currently running PHP %s, which is <a href="http://php.net/supported-versions.php" target="_blank">no longer officially supported</a>. Please contact your hosting provider to request that they update your PHP.', 'wp-press-kit'), PHP_VERSION); ?>
			</p>
		</div>
		<?php
	}
	add_action('admin_notices', 'wppresskit_php_version_admin_notice');
	return;
}

// include main plugin class
require_once(dirname(__FILE__).'/includes/plugin.php');
