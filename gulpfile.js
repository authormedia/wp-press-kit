var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var process = require('child_process');
var fs = require('fs');

function system(cmd) {
	process.execSync(cmd, function(error, stdout, stderr) {
		console.log(''+stdout+stderr);
	});
}

function rmdir(path) {
	try { var files = fs.readdirSync(path); } catch(e) { return; }
	if(files.length > 0) {
		for (var i = 0; i < files.length; i++) {
			var file = path + '/' + files[i];
			if (fs.statSync(file).isFile()) {
				fs.unlinkSync(file);
			} else {
				rmdir(file);
			}
		}
	}
	fs.rmdirSync(path);
}

gulp.task('admin-scripts', function() {
	gulp.src(['assets/js/admin/**/*.js'])
		.pipe(concat('admin.js'))
		.pipe(gulp.dest('js/admin'))
})

gulp.task('public-scripts', function() {
	gulp.src(['assets/js/public/**/*.js'])
		.pipe(concat('public.js'))
		.pipe(gulp.dest('js/public'))
})

gulp.task('admin-styles', function() {
	gulp.src(['assets/css/admin/admin.scss', 'assets/css/admin/notices.scss', 'assets/css/admin/forms.scss'])
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('css/admin'))
})

gulp.task('public-styles', function() {
	gulp.src(['assets/css/public/public.scss'])
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('css/public'))
})

gulp.task('lib-assets', function() {
	if(!fs.existsSync('bower_components')) { throw new Error('Bower packages not found (run bower install)') }

	cssfiles = [
		'bower_components/spectrum/spectrum.css',
		'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.css',
	]
	gulp.src(cssfiles).pipe(gulp.dest('css/lib'))

	jsfiles = [
		'bower_components/spectrum/spectrum.js',
		'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
		'bower_components/jquery-sortable/source/js/jquery-sortable.js',
	]
	gulp.src(jsfiles).pipe(gulp.dest('js/lib'))
})

gulp.task('default', ['lib-assets', 'admin-scripts', 'public-scripts', 'admin-styles', 'public-styles'], function() {})

gulp.task('watch', function() {
	gulp.watch('assets/js/admin/**', ['admin-scripts'])

	gulp.watch('assets/js/public/**', ['public-scripts'])

	gulp.watch('assets/css/admin/**', ['admin-styles'])

	gulp.watch('assets/css/public/**', ['public-styles'])
})

gulp.task('release', ['default'], function() {
	var slug = 'wppresskit';

	var contents = fs.readFileSync(slug+'.php', 'utf8');
	var version = contents.match(/^Version: (\d\.\d\.\d)$/m);
	if(!version) { throw new Error('Corrupted root plugin file'); }
	var release = slug+'.'+version[1]

	var ignore = {
		folders: ['assets', 'node_modules', 'bower_components'],
		files: ['bower.json', 'package.json', 'gulpfile.js'],
	}
	var ignores = ignore.folders.map(function(x) { return '!./'+x; }).concat(ignore.folders.map(function(x) { return '!./'+x+'/**/*'; })).concat(ignore.files.map(function(x) { return '!./'+x; }))
	gulp.src(['./**/*'].concat(ignores)).pipe(gulp.dest(slug)).on('end', function() {
		system('zip -rq '+release+'.zip '+slug)
		rmdir(slug)
	})
})
