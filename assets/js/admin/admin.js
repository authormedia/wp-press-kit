
window.WPPressKit = {};

WPPressKit.Plugin = {
	SLUG: 'wppresskit',
	slug: function(identifier) {
		delimeter = identifier.indexOf('-') === -1 ? '_' : '-';
		if(identifier[0] === delimeter) { identifier = identifier.substring(1); }
		return this.SLUG+delimeter+identifier;
	}
};

WPPressKit.Utils = {
	read_json_ele: function(element) {
		if(!element || !element.length) { return void(0); }
		try {
			return JSON.parse(element.html());
		} catch(e) {
			return void(0);
		}
	}
};

WPPressKit.Ajax = {
	do_action: function(action, variables, callback) {
		jQuery.post(ajaxurl, jQuery.extend({}, variables, {action: action}), callback);
	}
};
