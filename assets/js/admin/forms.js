jQuery(document).ready(function() {

	var Context = WPPressKit.Plugin;
	var Utils = WPPressKit.Utils;
	var Ajax = WPPressKit.Ajax;

	function create_file_selector(desired_attribute, input, preview) {
		var media_selector = wp.media({multiple: false});

		media_selector.on('select', function() {
			attachment = media_selector.state().get('selection').first().toJSON();
			if(preview) {
				output = '';
				output += '<div class="'+Context.slug('file-selector-preview-image')+'"><img src="'+(attachment.type == 'image' ? attachment.url : attachment.icon)+'"></div>';
				output += '<div class="'+Context.slug('file-selector-preview-text')+'">'+attachment.filename+'</div>';
				preview.html(output);
			}
			input.val(attachment[desired_attribute]).trigger('change');
		});

		return media_selector;
	}

	function reset_repeater_indexes(repeater) {
		var escape_regex = function(text) { return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'); };
		var escape_regex_replacement = function(text) { return text.replace('$', '$$'); };

		var data = Utils.read_json_ele(repeater.children('.'+Context.slug('repeater-data')));
		if(!data || !data.field || !data.id) { return; }
		var items = repeater.children('.'+Context.slug('repeater-list')).children('li');

		items.each(function(index, item) {
			item = jQuery(item);
			item.find('[name^="'+data.id+'_"], [id^="'+data.id+'_"]').each(function(_, subfield) {
				subfield = jQuery(subfield);
				if(subfield.attr('name')) { subfield.attr('name', subfield.attr('name').replace(new RegExp(escape_regex(data.id)+'_\\d(.*)'), escape_regex_replacement(data.id)+'_'+index+'$1')); }
				if(subfield.attr('id')) { subfield.attr('id', subfield.attr('id').replace(new RegExp(escape_regex(data.id)+'_\\d(.*)'), escape_regex_replacement(data.id)+'_'+index+'$1')); }
			});
		});

		var count = repeater.children('.'+Context.slug('repeater-count'));
		count.val(items.length);
	}

	// Todo: turn into event on element
	function init_elements(container) {
		container.find('.'+Context.slug('date-field-selector')).each(function(index, element) {
			var selector = jQuery(element);
			var input = selector.siblings('.'+Context.slug('date-field-input'));
			selector.datepicker({container: selector.closest('.'+Context.slug('form-fields'))}).on('changeDate', function(e) {
				input.val(Math.floor(e.date.getTime()*0.001)+e.date.getTimezoneOffset()*60);
			});
		});

		var adjustment;

		var options = {
			handle: '.'+Context.slug('repeater-item-handle'),
			onDragStart: function(item, container, _super) {
				var offset = item.offset(),
				pointer = container.rootGroup.pointer;

				adjustment = {
					left: pointer.left - offset.left,
					top: pointer.top - offset.top
				};

				_super(item, container);
			},
			onDrag: function(item, position) {
				item.css({
					left: position.left - adjustment.left,
					top: position.top - adjustment.top
				});
			},
			onDrop: function(item, container, _super) {
				reset_repeater_indexes(container.el.closest('.'+Context.slug('-repeater')));

				_super(item, container);
			}
		}

		container.find('.'+Context.slug('repeater-list')).sortable(options);

		return container;
	}

	// Repeater Form Field
	jQuery('.'+Context.slug('form-fields')).on('click', '.'+Context.slug('repeater-add-item'), function() {
		var button = jQuery(this);
		var repeater = button.closest('.'+Context.slug('-repeater'));
		var data = Utils.read_json_ele(repeater.children('.'+Context.slug('repeater-data')));
		if(!data || !data.field || !data.id) { return; }
		button.attr('disabled', 'disabled');
		var list = button.siblings('.'+Context.slug('repeater-list'));
		Ajax.do_action(Context.slug('repeater_form_field_add_item'), {field: JSON.stringify(data.field), id: data.id}, function(response) {
			init_elements(list.append(jQuery(response)));
			reset_repeater_indexes(repeater);
			button.removeAttr('disabled');
		});
	});
	jQuery('.'+Context.slug('form-fields')).on('click', '.'+Context.slug('repeater-item-remove'), function() {
		var item = jQuery(this).closest('.'+Context.slug('-repeater-item'));
		var repeater = item.closest('.'+Context.slug('-repeater'));
		item.remove();
		reset_repeater_indexes(repeater);
	});

	// File Form Field
	jQuery('.'+Context.slug('form-fields')).on('click', '.'+Context.slug('file-selector-select'), function(event) {

		var selector = jQuery(this).closest('.'+Context.slug('file-selector'));
		var file_selector = selector.data(Context.slug('media-selector'));
		if(!file_selector) {
			file_selector = create_file_selector('id', selector.find('.'+Context.slug('file-selector-input')), selector.find('.'+Context.slug('file-selector-preview')));
			selector.data(Context.slug('media-selector'), file_selector);
		}
		file_selector.open();

		return false;
	});
	jQuery('.'+Context.slug('form-fields')).on('click', '.'+Context.slug('file-selector-clear'), function(event) {
		var selector = jQuery(this).closest('.'+Context.slug('file-selector'));
		selector.find('.'+Context.slug('file-selector-preview')).html('');
		selector.find('.'+Context.slug('file-selector-input')).val('');
		return false;
	});

	// File URL Form Field
	jQuery('.'+Context.slug('form-fields')).on('click', '.'+Context.slug('file-url-field-select'), function(event) {

		var container = jQuery(this).siblings('.'+Context.slug('file-url-field-input-container'));
		var file_selector = container.data(Context.slug('media-selector'));
		if(!file_selector) {
			file_selector = create_file_selector('url', container.find('.'+Context.slug('file-url-field-input')));
			container.data(Context.slug('media-selector'), file_selector);
		}
		file_selector.open();

		return false;
	});

	// Term & Taxonomy Form Field
	jQuery('.'+Context.slug('form-fields')).on('change', '.'+Context.slug('taxonomy-term-field-taxonomy'), function(event) {

		var taxonomy = jQuery(this);
		var input = jQuery(this).siblings('.'+Context.slug('taxonomy-term-field-input'));
		var term = jQuery(this).siblings('.'+Context.slug('taxonomy-term-field-term'));

		input.val('');
		term.attr('disabled', 'disabled');
		Ajax.do_action(Context.slug('taxonomy_term_form_field_get_terms'), {taxonomy: taxonomy.val()}, function(response) {
			term.replaceWith(jQuery(response));
			term.removeAttr('disabled');
		});

		return false;
	});
	jQuery('.'+Context.slug('form-fields')).on('change', '.'+Context.slug('taxonomy-term-field-term'), function(event) {

		var term = jQuery(this);
		var input = jQuery(this).siblings('.'+Context.slug('taxonomy-term-field-input'));
		var taxonomy = jQuery(this).siblings('.'+Context.slug('taxonomy-term-field-taxonomy'));

		input.val(JSON.stringify({term: term.val(), taxonomy: taxonomy.val()}));

		return false;
	});

	init_elements(jQuery('.'+Context.slug('form-fields')));

});
