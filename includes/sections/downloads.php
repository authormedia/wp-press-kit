<?php

namespace WPPressKit;

final class DownloadsSection extends Section {

	const ID = 'downloads';

	protected function __construct() {}

	protected function name() { return __('Downloads', Plugin::ID); }

	protected function priority() { return 80; }

	protected function admin_fields() {
		return array(
			'presskit' => array('type' => 'file_url', 'name' => __('Press Kit File', Plugin::ID)),
			'downloads' => array(
				'type' => 'repeater',
				'name' => __('Related Downloads', Plugin::ID),
				'field' => array(
					'type' => 'group',
					'fields' => array(
						'name' => array('type' => 'text', 'name' => __('Download Name', Plugin::ID)),
						'url' => array('type' => 'file_url', 'name' => __('Download URL', Plugin::ID)),
					),
				),
				'desc' => __("This is where you add your PDF press packet, PDF speaker kit, HD video files, or any other assets that don't fit into one of the categories above.", Plugin::ID),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['presskit']) or !empty($data['downloads'])) {
			$output .= '<section class="'.Plugin::slug('downloads-section').'">';
			if(!empty($data['presskit'])) {
				$output .= '<div class="'.Plugin::slug('download-presskit-container').'">';
				$output .= '<a class="'.Plugin::slug('download-presskit').'" href="'.$data['presskit'].'" target="_blank">'.__('Download Press Kit', Plugin::ID).'</a>';
				$output .= '</div>';
			}
			if(!empty($data['downloads'])) {
				$output .= '<h3>'.__('Related Downloads', Plugin::ID).'</h2>';
				$output .= '<ul class="'.Plugin::slug('-downloads').'">';
				foreach($data['downloads'] as $download) {
					$output .= '<li class="'.Plugin::slug('-download').'">';
					$output .= '<a class="'.Plugin::slug('download-link').'" href="'.$download['url'].'" target="_blank">'.htmlspecialchars($download['name'], ENT_QUOTES).'</a>';
					$output .= '</li>';
				}
				$output .= '</ul>';
			}
			$output .= '</section>';
		}

		return $output;
	}
}

DownloadsSection::add_action(Plugin::slug('init'), 'init');
