<?php

namespace WPPressKit;

final class MyBookTableSection extends Section {

	const ID = 'mybooktable';

	protected function __construct() {}

	protected function name() { return __('MyBookTable', Plugin::ID); }

	protected function priority() { return 90; }

	protected function admin_fields() {
		return array(
			'to_be_implemented' => array(
				'type' => 'text',
				'name' => __('To be implemented', Plugin::ID),
				'default' => __('To be implemented', Plugin::ID),
			),
		);
	}

	protected function render($post) {
		$output = '';
		$output .= '<section>To be implemented</section>';
		return $output;
	}
}

//MyBookTableSection::add_action(Plugin::slug('init'), 'init');
