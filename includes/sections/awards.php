<?php

namespace WPPressKit;

final class AwardsSection extends Section {

	const ID = 'awards';

	protected function __construct() {}

	protected function name() { return __('Awards', Plugin::ID); }

	protected function priority() { return 60; }

	protected function admin_fields() {
		return array(
			'awards' => array(
				'type' => 'repeater',
				'field' => array(
					'type' => 'group',
					'fields' => array(
						'name' => array('type' => 'text', 'name' => __('Award Name', Plugin::ID)),
						'image' => array('type' => 'file', 'name' => __('Image', Plugin::ID)),
						'content' => array('type' => 'textarea', 'name' => __('Description', Plugin::ID)),
						'organization' => array('type' => 'text', 'name' => __('Organization', Plugin::ID)),
						'organization_url' => array('type' => 'text', 'name' => __('Organization URL', Plugin::ID)),
						'date' => array('type' => 'date', 'name' => __('Date', Plugin::ID)),
					),
				),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['awards']) and is_array($data['awards'])) {
			$output .= '<section class="'.Plugin::slug('awards-section').'">';
			$output .= '<h2>'.__('Awards', Plugin::ID).'</h2>';
			$output .= '<ul class="'.Plugin::slug('-awards').'">';
			foreach($data['awards'] as $award) {
				$output .= '<li class="'.Plugin::slug('-award').'">';

				$image_output = wp_get_attachment_image($award['image'], Plugin::slug('medium'));
				if(!empty($image_output)) { $output .= '<div class="'.Plugin::slug('award-image').'">'.$image_output.'</div>'; }
				$output .= '<div class="'.Plugin::slug('award-details').'">';
				$output .= '<div class="'.Plugin::slug('award-title').'">'.$award['name'].'</div>';
				$output .= '<div class="'.Plugin::slug('award-content').'">'.\do_shortcode($award['content']).'</div>';
				$output .= '<div class="'.Plugin::slug('award-meta').'">';
				$organization_output = !empty($award['organization_url']) ? '<a href="'.$award['organization_url'].'">'.$award['organization'].'</a>' : $award['organization'];
				if(!empty($award['organization'])) { $output .= '<div class="'.Plugin::slug('award-organization').'">'.$organization_output.'</div>'; }
				if(!empty($award['date'])) { $output .= '<div class="'.Plugin::slug('award-date').'">'.date('F j, Y', $award['date']).'</div>'; }
				$output .= '</div>';
				$output .= '</div>';

				$output .= '</li>';
			}
			$output .= '</ul>';
			$output .= '</section>';
		}

		return $output;
	}
}

AwardsSection::add_action(Plugin::slug('init'), 'init');
