<?php

namespace WPPressKit;

final class TwitterSection extends Section {

	const ID = 'twitter';

	protected function __construct() {}

	protected function name() { return __('Twitter', Plugin::ID); }

	protected function priority() { return 70; }

	protected function admin_fields() {
		return array(
			'to_be_implemented' => array(
				'type' => 'text',
				'name' => __('To be implemented', Plugin::ID),
				'default' => __('To be implemented', Plugin::ID),
			),
		);
	}

	protected function render($post) {
		$output = '';
		$output .= '<section>To be implemented</section>';
		return $output;
	}
}

//TwitterSection::add_action(Plugin::slug('init'), 'init');
