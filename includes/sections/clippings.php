<?php

namespace WPPressKit;

final class ClippingsSection extends Section {

	const ID = 'clippings';

	protected function __construct() {}

	protected function name() { return __('Press Clippings', Plugin::ID); }

	protected function priority() { return 40; }

	protected function admin_fields() {
		return array(
			'clippings' => array(
				'type' => 'repeater',
				'field' => array(
					'type' => 'group',
					'fields' => array(
						'image' => array('type' => 'file', 'name' => __('Photo', Plugin::ID)),
						'content' => array('type' => 'textarea', 'name' => __('Clipping', Plugin::ID)),
						'publication' => array('type' => 'text', 'name' => __('Publication', Plugin::ID)),
						'publication_url' => array('type' => 'text', 'name' => __('Publication URL', Plugin::ID)), // TODO: Add URL validation
					),
				),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['clippings']) and is_array($data['clippings'])) {
			$output .= '<section class="'.Plugin::slug('clippings-section').'">';
			$output .= '<h2>'.__('Press Clippings', Plugin::ID).'</h2>';
			$output .= '<ul class="'.Plugin::slug('-clippings').'">';
			//$limit = 0;
			foreach($data['clippings'] as $clipping) {
				$output .= '<li class="'.Plugin::slug('-clipping').'">';

				$image_output = wp_get_attachment_image($clipping['image'], Plugin::slug('medium'));
				if(!empty($image_output)) { $output .= '<div class="'.Plugin::slug('clipping-image').'">'.$image_output.'</div>'; }
				$output .= '<div class="'.Plugin::slug('clipping-details').'">';
				$output .= '<div class="'.Plugin::slug('clipping-content').'">'.\do_shortcode($clipping['content']).'</div>';
				$output .= '<div class="'.Plugin::slug('clipping-meta').'">';
				$publication_output = !empty($clipping['publication_url']) ? '<a href="'.$clipping['publication_url'].'">'.$clipping['publication'].'</a>' : $clipping['publication'];
				if(!empty($clipping['publication'])) { $output .= '<div class="'.Plugin::slug('clipping-publication').'">'.$publication_output.'</div>'; }
				$output .= '</div>';
				$output .= '</div>';

				$output .= '</li>';
				//$limit++; if($limit >= $data['max_shown']) { break; }
			}
			$output .= '</ul>';
			// if(count($data['clippings']) > $data['max_shown']) {
			// 	$permalink = get_permalink();
			// 	$more_url = $permalink.(strpos($permalink, '?') === false ? '?' : '&').Plugin::slug('page').'=clippings';
			// 	$output .= '<a href="'.$more_url.'" class="'.Plugin::slug('more').'">'.__('More Press Clippings', Plugin::ID).'</a>';
			// }
			$output .= '</section>';
		}

		return $output;
	}
}

ClippingsSection::add_action(Plugin::slug('init'), 'init');
