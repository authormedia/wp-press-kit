<?php

namespace WPPressKit;

final class FAQSection extends Section {

	const ID = 'faq';

	protected function __construct() {}

	protected function name() { return __('Frequently Asked Questions', Plugin::ID); }

	protected function priority() { return 50; }

	protected function admin_fields() {
		return array(
			'questions' => array(
				'type' => 'repeater',
				'name' => __('Questions', Plugin::ID),
				'field' => array(
					'type' => 'group',
					'fields' => array(
						'question' => array('type' => 'text', 'name' => __('Questions', Plugin::ID)),
						'answer' => array('type' => 'textarea', 'name' => __('Answer', Plugin::ID)),
					),
				),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['questions']) and is_array($data['questions'])) {
			$output .= '<section class="'.Plugin::slug('faq-section').'">';
			$output .= '<h2>'.__('Frequently Asked Questions', Plugin::ID).'</h2>';
			$output .= '<ul class="'.Plugin::slug('-questions').'">';
			foreach($data['questions'] as $question) {
				$output .= '<li class="'.Plugin::slug('-question').'">';
				$output .= '<div class="'.Plugin::slug('question-text').'">'.$question['question'].'</div>';
				$output .= '<div class="'.Plugin::slug('question-answer').'">'.$question['answer'].'</div>';
				$output .= '</li>';
			}
			$output .= '</ul>';
			$output .= '</section>';
		}

		return $output;
	}
}

FAQSection::add_action(Plugin::slug('init'), 'init');
