<?php

namespace WPPressKit;

final class NewsSection extends Section {

	const ID = 'news';

	protected function __construct() {}

	protected function name() { return __('News Releases', Plugin::ID); }

	protected function priority() { return 30; }

	protected function admin_fields() {
		return array(
			'taxonomy_term' => array(
				'type' => 'taxonomy_term',
				'name' => __('Pull Posts From', Plugin::ID),
			),
			'num_posts' => array(
				'type' => 'number',
				'integral' => true,
				'name' => __('Posts to Show', Plugin::ID),
				'default' => 5,
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['taxonomy_term']) and is_array($data['taxonomy_term'])) {
			$query = new \WP_Query(array(
				'post_type' => 'post',
				'tax_query' => array(
					array(
						'taxonomy' => $data['taxonomy_term']['taxonomy'],
						'field' => 'slug',
						'terms' => $data['taxonomy_term']['term'],
					),
				),
				'posts_per_page' => $data['num_posts']+1,
			));
			$posts = $query->posts;

			if(is_array($posts) and !empty($posts)) {
				$output .= '<section class="'.Plugin::slug('news-section').'">';
				$output .= '<h2>'.__('News Releases', Plugin::ID).'</h2>';
				$output .= '<ul class="'.Plugin::slug('-news').'">';
				$limit = 0;
				foreach($posts as $post) {
					$output .= '<li class="'.Plugin::slug('news-item').'">';
					$output .= '<div class="'.Plugin::slug('news-title').'"><a href="'.get_the_permalink($post).'">'.get_the_title($post).'</a></div>';
					$output .= '<div class="'.Plugin::slug('news-date').'">'.get_the_date('', $post).'</div>';
					$output .= '</li>';
					$limit++; if($limit >= $data['num_posts']) { break; }
				}
				$output .= '</ul>';
				if(count($posts) > $data['num_posts']) {
					$term_url = get_term_link($data['taxonomy_term']['term'], $data['taxonomy_term']['taxonomy']);
					$output .= '<a href="'.$term_url.'" class="'.Plugin::slug('more').'">'.__('More News Releases', Plugin::ID).'</a>';
				}
				$output .= '</section>';
			}
		}

		return $output;
	}
}

NewsSection::add_action(Plugin::slug('init'), 'init');
