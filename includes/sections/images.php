<?php

namespace WPPressKit;

final class ImagesSection extends Section {

	const ID = 'images';

	protected function __construct() {}

	protected function init() {
		parent::init();
		add_image_size(Plugin::slug('images'), 1000, 100, false);
	}

	protected function name() { return __('High Resolution Images', Plugin::ID); }

	protected function priority() { return 10; }

	protected function admin_fields() {
		return array(
			'title' => array(
				'type' => 'text',
				'name' => __('Section Title', Plugin::ID),
				'default' => __('High Resolution Images', Plugin::ID),
			),
			'images' => array(
				'type' => 'repeater',
				'name' => __('Images', Plugin::ID),
				'field' => array(
					'type' => 'file',
					'name' => __('Image', Plugin::ID),
				),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['images']) and is_array($data['images'])) {
			$output .= '<section class="'.Plugin::slug('images-section').'">';
			if(!empty($data['title'])) { $output .= '<h2>'.htmlspecialchars($data['title'], ENT_QUOTES).'</h2>'; }
			$output .= '<div class="desc">'.__('Click to download a high resolution version.', Plugin::ID).'</div>';
			$output .= '<ul class="'.Plugin::slug('-images').'">';
			$images = $data['images'];
			foreach($images as $image) {
				$image_output = wp_get_attachment_image($image, Plugin::slug('images'));
				if($image_output) {
					$download_url = wp_get_attachment_image_src($image, 'full')[0];
					$output .= '<li class="'.Plugin::slug('-image').'">';
					$output .= '	<a href="'.$download_url.'" download>'.$image_output.'</a>';
					$output .= '</li>';
				}
			}
			$output .= '</ul>';
			$output .= '</section>';
		}

		return $output;
	}
}

ImagesSection::add_action(Plugin::slug('init'), 'init');
