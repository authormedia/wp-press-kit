<?php

namespace WPPressKit;

final class AboutSection extends Section {

	const ID = 'about';

	protected function __construct() {}

	protected function init() {
		parent::init();

		add_filter(Plugin::slug('content'), 'wptexturize');
		add_filter(Plugin::slug('content'), 'convert_smilies');
		add_filter(Plugin::slug('content'), 'convert_chars');
		add_filter(Plugin::slug('content'), 'wpautop');
		add_filter(Plugin::slug('content'), 'shortcode_unautop');
		add_filter(Plugin::slug('content'), 'do_shortcode');
	}

	protected function name() { return __('About', Plugin::ID); }

	protected function priority() { return 20; }

	protected function admin_fields() {
		return array(
			'title' => array(
				'type' => 'text',
				'name' => __('Section Title', Plugin::ID),
				'default' => __('About You', Plugin::ID),
			),
			'content' => array(
				'type' => 'wysiwyg',
				'name' => __('Content', Plugin::ID),
				'desc' => __('Assume the journalist reading this knows nothing about you and your organization. One trick to get you started is to re-write your about page as if it were a press release. Feel free to add some quotes and make sure to write it in the third person.', Plugin::ID),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);

		if(!empty($data['content'])) {
			$output .= '<section class="'.Plugin::slug('about-section').'">';
			if(!empty($data['title'])) { $output .= '<h2>'.htmlspecialchars($data['title'], ENT_QUOTES).'</h2>'; }
			$output .= '<div class="'.Plugin::slug('about-content').'">';
			$output .= apply_filters(Plugin::slug('content'), $data['content']);
			$output .= '</div>';
			$output .= '</section>';
		}

		return $output;
	}
}

AboutSection::add_action(Plugin::slug('init'), 'init');
