<?php

namespace WPPressKit;

final class ContactSection extends Section {

	const ID = 'contact';

	protected function __construct() {}

	protected function name() { return __('Press Contact', Plugin::ID); }

	protected function priority() { return 30; }

	protected function admin_fields() {
		return array(
			'name' => array(
				'type' => 'text',
				'name' => __('Name', Plugin::ID),
			),
			'phone' => array(
				'type' => 'text',
				'name' => __('Phone', Plugin::ID),
			),
			'email' => array(
				'type' => 'text',
				'name' => __('Email', Plugin::ID),
			),
		);
	}

	protected function render($post) {
		$output = '';

		$data = get_post_meta($post->ID, Plugin::slug(self::ID), true);
		if(!empty($data['name']) and !empty($data['phone']) and !empty($data['email'])) {
			$output .= '<section class="'.Plugin::slug('contact-section').'">';
			$output .= '<h2>'.__('Press Contact', Plugin::ID).'</h2>';
			if(!empty($data['name'])) { $output .= '<div class="'.Plugin::slug('contact-name').'">'.htmlspecialchars($data['name'], ENT_QUOTES).'</div>'; }
			if(!empty($data['phone'])) { $output .= '<div class="'.Plugin::slug('contact-phone').'">'.htmlspecialchars($data['phone'], ENT_QUOTES).'</div>'; }
			if(!empty($data['email'])) { $output .= '<div class="'.Plugin::slug('contact-email').'">'.htmlspecialchars($data['email'], ENT_QUOTES).'</div>'; }
			$output .= '</section>';
		}

		return $output;
	}
}

ContactSection::add_action(Plugin::slug('init'), 'init');
