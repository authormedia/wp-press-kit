<?php

namespace WPPressKit;

final class MySpeakingEventsSection extends Section {

	const ID = 'myspeakingevents';

	protected function __construct() {}

	protected function name() { return __('MySpeakingEvents', Plugin::ID); }

	protected function priority() { return 100; }

	protected function admin_fields() {
		return array(
			'to_be_implemented' => array(
				'type' => 'text',
				'name' => __('To be implemented', Plugin::ID),
				'default' => __('To be implemented', Plugin::ID),
			),
		);
	}

	protected function render($post) {
		if(defined("MSE_VERSION")) {
			$num_upcoming_events = get_post_meta($post->ID, 'msp_num_upcoming_events', true);
			if(!empty($num_upcoming_events)) {
				$output .= '<section class="msp-upcoming-events-section">';
				$output .= '<h2>'.get_post_meta($post->ID, 'msp_upcoming_events_title', true).'</h2>';
				$events = new WP_Query(array('post_type' => 'mse_event', 'orderby' => 'meta_value_num', 'posts_per_page' => $num_upcoming_events, 'meta_query' => array(
					array(
						'key' => 'mse_time_start',
						'value' => time()-172800,
						'type' => 'numeric',
						'compare' => '>',
					)
				)));
				if(is_array($events->posts) and count($events->posts) > 0) {
					foreach($events->posts as $event) {
						$output .= '<div class="msp-event">';
						$output .= '<a href="'.get_permalink($event->ID).'" class="msp-event-title">'.$event->post_title.'</a>';
						$output .= ' - '.mse_get_event_location($event->ID).', '.mse_get_event_time_start($event->ID, 'm/d/Y');
						$output .= '</div>';
					}
					if(mse_get_setting('upcoming_events_page') != 0 and get_page(mse_get_setting('upcoming_events_page'))) {
						$output .= '<a href="'.get_permalink(mse_get_setting('upcoming_events_page')).'" class="msp-more-page">All Upcoming Events</a>';
					}
				} else {
					$output .= '<div class="msp-no-events">No events to list</div>';
				}
				$output .= '</section>';

				if(get_post_meta($post->ID, 'msp_upcoming_events_call_to_action', true)) {
					$output .= '<div class="msp-call-to-action">';
					$output .= '	<a href="'.get_post_meta($post->ID, 'msp_book_to_speak_url', true).'" class="msp-button">'.$button_text.'</a>';
					$output .= '</div>';
				}
			}

			$num_past_events = get_post_meta($post->ID, 'msp_num_past_events', true);
			if(!empty($num_past_events)) {
				$output .= '<section class="msp-past-events-section">';
				$output .= '<h2>'.get_post_meta($post->ID, 'msp_past_events_title', true).'</h2>';
				$events = new WP_Query(array('post_type' => 'mse_event', 'orderby' => 'meta_value_num', 'posts_per_page' => $num_past_events, 'meta_query' => array(
					array(
						'key' => 'mse_time_start',
						'value' => time()-172800,
						'type' => 'numeric',
						'compare' => '<',
					)
				)));
				if(is_array($events->posts) and count($events->posts) > 0) {
					foreach($events->posts as $event) {
						$output .= '<div class="msp-event">';
						$output .= '<a href="'.get_permalink($event->ID).'" class="msp-event-title">'.$event->post_title.'</a>';
						$output .= ' - '.mse_get_event_location($event->ID).', '.mse_get_event_time_start($event->ID, 'm/d/Y');
						$output .= '</div>';
					}
					if(mse_get_setting('past_events_page') != 0 and get_page(mse_get_setting('past_events_page'))) {
						$output .= '<a href="'.get_permalink(mse_get_setting('past_events_page')).'" class="msp-more-page">All Past Events</a>';
					}
				} else {
					$output .= '<div class="msp-no-events">No events to list</div>';
				}
				$output .= '</section>';

				if(get_post_meta($post->ID, 'msp_past_events_call_to_action', true)) {
					$output .= '<div class="msp-call-to-action">';
					$output .= '	<a href="'.get_post_meta($post->ID, 'msp_book_to_speak_url', true).'" class="msp-button">'.$button_text.'</a>';
					$output .= '</div>';
				}
			}
		}
	}
}

//MySpeakingEventsSection::add_action(Plugin::slug('init'), 'init');
