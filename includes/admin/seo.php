<?php

namespace WPPressKit;

final class PressKits_SEO_Admin extends Singleton {

	protected function __construct() {}

	protected function init() {
		if(!defined('WPSEO_FILE')) {
			//Custom SEO Metabox
			self::add_action(PressKits::save_post_action(), 'save_seo_metabox');
			self::add_action('add_meta_boxes', 'add_seo_metabox', 9999);
		}
	}

	protected function seo_metabox($post) {
		$presskit = PressKits::get_post($post);
	?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				var title = jQuery('#<?php echo(Plugin::slug('seo_title')); ?>');
				var title_length = jQuery('#<?php echo(Plugin::slug('seo_title')); ?>_length');
				title.on('input', function() {
					var chars_left = 70-title.val().length;
					title_length.text(chars_left);
					if(chars_left < 0) {
						title_length.addClass('bad');
					} else {
						title_length.removeClass('bad');
					}
				});
				title.trigger('input');

				var metadesc = jQuery('#<?php echo(Plugin::slug('seo_metadesc')); ?>');
				var metadesc_length = jQuery('#<?php echo(Plugin::slug('seo_metadesc')); ?>_length');
				metadesc.on('input', function() {
					var chars_left = 156-metadesc.val().length;
					metadesc_length.text(chars_left);
					if(chars_left < 0) {
						metadesc_length.addClass('bad');
					} else {
						metadesc_length.removeClass('bad');
					}
				});
				metadesc.trigger('input');
			});
		</script>

		<table class="form-table <?php echo(Plugin::slug('seo_metabox')); ?>">
			<tbody>
				<tr>
					<th scope="row">
						<label for="<?php echo(Plugin::slug('seo_title')); ?>"><?php _e('SEO Title:', Plugin::ID); ?></label>
					</th>
					<td>
						<input type="text" placeholder="" id="<?php echo(Plugin::slug('seo_title')); ?>" name="<?php echo(Plugin::slug('seo_title')); ?>" value="<?php echo($presskit->get_meta('seo_title')); ?>" class="large-text"><br>
						<p><?php printf(__('Title display in search engines is limited to 70 chars, %s chars left.', Plugin::ID), '<span id="'.Plugin::slug('seo_title').'_length">70</span>'); ?></p>
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="<?php echo(Plugin::slug('seo_metadesc')); ?>"><?php _e('Meta Description:', Plugin::ID); ?></label></th>
					<td>
						<textarea class="large-text" rows="3" id="<?php echo(Plugin::slug('seo_metadesc')); ?>" name="<?php echo(Plugin::slug('seo_metadesc')); ?>"><?php echo($presskit->get_meta('seo_metadesc')); ?></textarea>
						<p><?php printf(__('The <code>meta</code> description will be limited to 156 chars, %s chars left.', Plugin::ID), '<span id="'.Plugin::slug('seo_metadesc').'_length">156</span>'); ?></p>
					</td>
				</tr>
			</tbody>
		</table>
	<?php
	}

	protected function save_seo_metabox($post_id) {
		$presskit = PressKits::get_post($post_id);
		if(isset($_REQUEST[Plugin::slug('seo_title')])) { $presskit->set_meta(Plugin::slug('seo_title'), $_REQUEST[Plugin::slug('seo_title')]); }
		if(isset($_REQUEST[Plugin::slug('seo_metadesc')])) { $presskit->set_meta(Plugin::slug('seo_metadesc'), $_REQUEST[Plugin::slug('seo_metadesc')]); }
	}

	protected function add_seo_metabox() {
		add_meta_box(Plugin::slug('seo'), __('SEO Information', Plugin::ID), self::funcname('seo_metabox'), PressKits::POST_TYPE, 'normal', 'default');
	}
}

PressKits_SEO_Admin::add_action(Plugin::slug('init'), 'init');
