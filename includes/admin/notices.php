<?php

namespace WPPressKit;

final class Notices extends Singleton {

	/*---------------------------------------------------------*/
	/* Protected Functions                                     */
	/*---------------------------------------------------------*/

	protected function __construct() {}

	protected function init() {
		Settings::add('email_subscribe_notice');
		self::add_action('admin_enqueue_scripts', 'do_admin_enqueue_scripts');
		self::add_action('current_screen', 'register');
		self::add_action('wp_ajax_'.Plugin::slug('allow_tracking_notice_response'), 'do_allow_tracking_notice_response');
		self::add_action('wp_ajax_'.Plugin::slug('email_subscribe_notice_response'), 'do_email_subscribe_notice_response');
	}

	protected function do_admin_enqueue_scripts() {
		wp_enqueue_style(Plugin::slug('notices'), Plugin::url('css/admin/notices.css'), array(), Plugin::version());
	}

	protected function register() {
		$screen = get_current_screen();
		if(count(PressKits::get_posts()) > 0 and $screen->id == PressKits::POST_TYPE and current_user_can('manage_options')) {
			if(Settings::get('allow_tracking') === null) {
				wp_enqueue_script('wp-pointer');
				wp_enqueue_style('wp-pointer');
				self::add_action('admin_print_footer_scripts', 'show_allow_tracking_notice');
			} else if(Settings::get('email_subscribe_notice') !== 'done') {
				wp_enqueue_script('wp-pointer');
				wp_enqueue_style('wp-pointer');
				self::add_action('admin_print_footer_scripts', 'show_email_subscribe_notice');
			}
		}
	}

	protected function show_allow_tracking_notice() {
		$content  = '<h3>'.htmlspecialchars(sprintf(__('Help improve %s', Plugin::ID), Plugin::NAME), ENT_QUOTES).'</h3>';
		$content .= '<p>'.htmlspecialchars(sprintf(__('You can help make %s even better and easier to use by allowing it to gather anonymous statistics about how you use the plugin.', Plugin::ID), Plugin::NAME), ENT_QUOTES).'</p>';
		$content .= '<div class="wp-pointer-buttons">';
		$content .= '<a class="button button-primary wp-pointer-yes" style="float:left">'.htmlspecialchars(__("Let's do it!", Plugin::ID), ENT_QUOTES).'</a>';
		$content .= '<a class="button button-secondary wp-pointer-no">'.htmlspecialchars(__("I'd Rather Not", Plugin::ID), ENT_QUOTES).'</a>';
		$content .= '</div>';

		?>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				var action = '<?php echo(Plugin::slug('allow_tracking_notice_response')); ?>';
				var pointer_class = '<?php echo(Plugin::slug('allow-tracking-notice')); ?>';
				var content = jQuery('<div>').html('<?php echo($content); ?>');

				content.on('click', '.wp-pointer-yes', function() {
					content.find('.button').attr('disabled', 'disabled');
					jQuery.post(ajaxurl,
						{
							action: action,
							allow_tracking: 'yes',
						},
						function(response) {
							content.html(response);
						}
					);
				});
				content.on('click', '.wp-pointer-no', function() {
					jQuery.post(ajaxurl, {action: action, allow_tracking: 'no'});
					jQuery('#wpadminbar').pointer('close');
				});
				content.on('click', '.wp-pointer-close', function() {
					jQuery('#wpadminbar').pointer('close');
				});

				jQuery('#wpadminbar').pointer({
					position: {edge: 'top', align: 'center'},
					buttons: function() {},
					pointerClass: pointer_class,
					content: ' ',
				}).pointer('open');
				jQuery('.'+pointer_class+' .wp-pointer-content').empty().append(content);
			});
		</script>
		<?php
	}

	protected function do_allow_tracking_notice_response() {
		if(empty($_REQUEST['allow_tracking'])) { die(); }
		if($_REQUEST['allow_tracking'] === 'yes') {
			Settings::set('allow_tracking', 'yes');
			Tracking::track_event('tracking_allowed', true);
			Tracking::send_data();

			$content  = '<h3>'.sprintf(__('Help improve %s', Plugin::ID), Plugin::NAME).'</h3>';
			$content .= '<p>'.__("Thanks! You're the best!", Plugin::ID).'</p>';
			$content .= '<div class="wp-pointer-buttons">';
			$content .= '<a class="wp-pointer-close button-secondary">'.__('Close', Plugin::ID).'</a>';
			$content .= '</div>';
			echo($content);
		} else {
			Tracking::track_event('tracking_denied', true);
			Settings::set('allow_tracking', 'no');
		}
		die();
	}

	protected function show_email_subscribe_notice() {
		$current_user = wp_get_current_user();
		$email = $current_user->user_email;

		$content  = '<h3>'.htmlspecialchars(__('Get Branding Tips, Marketing Advice and Plugin Updates', Plugin::ID), ENT_QUOTES).'</h3>';
		$content .= '<p>'.htmlspecialchars(__("Join over 7,000 other authors on the Author Media's award winning newsletter. AuthorMedia.com has been frequently recommended by Writer's Digest as one of the most helpful websites for authors. You can unsubscribe at anytime with just one click.", Plugin::ID), ENT_QUOTES).'</p>';
		$content .= '<form style="margin:15px"><input type="email" autocapitalize="off" autocorrect="off" placeholder="you@example.com" value="'.$email.'" style="width: 100%"></form>';
		$content .= '<div class="wp-pointer-buttons">';
		$content .= '<a class="button button-primary wp-pointer-yes" style="float:left">'.htmlspecialchars(__("Let's do it!", Plugin::ID), ENT_QUOTES).'</a>';
		$content .= '<a class="button button-secondary wp-pointer-no">'.htmlspecialchars(__('No, thanks', Plugin::ID), ENT_QUOTES).'</a>';
		$content .= '</div>';

		?>
		<script type="text/javascript">
			jQuery(document).ready(function () {
				var action = '<?php echo(Plugin::slug('email_subscribe_notice_response')); ?>';
				var pointer_class = '<?php echo(Plugin::slug('email-subscribe-notice')); ?>';
				var content = jQuery('<div>').html('<?php echo($content); ?>');

				content.on('submit', 'form', function() {
					if(!/^.+@.+$/.test(content.find('input').val())) {
						content.find('input').addClass('error').focus();
					} else {
						content.find('.button, input').attr('disabled', 'disabled');
						jQuery.post(ajaxurl,
							{
								action: action,
								subscribe: 'yes',
								email: content.find('input').val(),
							},
							function(response) {
								content.html(response);
							}
						);
					}
					return false;
				});

				content.on('click', '.wp-pointer-yes', function() {
					content.find('form').submit();
				});
				content.on('click', '.wp-pointer-no', function() {
					jQuery.post(ajaxurl, {action: action, subscribe: 'no'});
					jQuery('#wpadminbar').pointer('close');
				});
				content.on('click', '.wp-pointer-close', function() {
					jQuery('#wpadminbar').pointer('close');
				});

				jQuery('#wpadminbar').pointer({
					position: {edge: 'top', align: 'center'},
					buttons: function() {},
					pointerClass: pointer_class,
					content: ' ',
				}).pointer('open');
				jQuery('.'+pointer_class+' .wp-pointer-content').empty().append(content);
			});
		</script>
		<?php
	}

	protected function do_email_subscribe_notice_response() {
		if(empty($_REQUEST['subscribe'])) { die(); }
		if($_REQUEST['subscribe'] === 'yes') {
			Tracking::track_event('email_subscribe_accepted');

			$email = $_POST['email'];
			wp_remote_post('http://AuthorMedia.us1.list-manage1.com/subscribe/post', array(
				'body' => array(
					'u' => 'b7358f48fe541fe61acdf747b',
					'id' => '6b5a675fcf',
					'MERGE0' => $email,
					'MERGE1' => '',
					'MERGE3' => '',
					'group[3045][4194304]' => 'on',
				)
			));

			$content  = '<h3>'.__('Get Branding Tips, Marketing Advice and Plugin Updates', Plugin::ID).'</h3>';
			$content .= '<p>'.__('Thank you for subscribing! Please check your inbox for a confirmation letter.', Plugin::ID).'</p>';
			$content .= '<div class="wp-pointer-buttons">';

			$email_title = '';
			$email_link = '';
			if(strpos($email , '@yahoo') !== false) {
				$email_title = __('Go to Yahoo! Mail', Plugin::ID);
				$email_link = 'https://mail.yahoo.com/';
			} else if(strpos($email, '@hotmail') !== false) {
				$email_title = __('Go to Hotmail', Plugin::ID);
				$email_link = 'https://www.hotmail.com/';
			} else if(strpos($email, '@gmail') !== false) {
				$email_title = __('Go to Gmail', Plugin::ID);
				$email_link = 'https://mail.google.com/';
			} else if(strpos($email, '@aol') !== false) {
				$email_title = __('Go to AOL Mail', Plugin::ID);
				$email_link = 'https://mail.aol.com/';
			}
			if(!empty($email_title)) {
				$content .= '<a class="button-primary" style="float:left" href="'.$email_link.'" target="_blank">'.$email_title.'</a>';
			}

			$content .= '<a class="wp-pointer-close button-secondary">'.__('Close', Plugin::ID).'</a>';
			$content .= '</div>';
			echo($content);
		} else {
			Tracking::track_event('email_subscribe_denied');
		}
		Settings::set('email_subscribe_notice', 'done');
		die();
	}
}

Notices::add_action(Plugin::slug('init'), 'init');
