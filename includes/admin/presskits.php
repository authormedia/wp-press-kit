<?php

namespace WPPressKit;

final class PressKits_Admin extends Singleton {

	protected function __construct() {}

	protected function init() {
		self::add_action('admin_enqueue_scripts', 'enqueue_resources');
		self::add_sections_metaboxes();
	}

	protected function add_sections_metaboxes() {
		$sections = Sections::get_sections();

		foreach ($sections as $id => $section) {
			$metabox = new Forms\PostMetabox($id, $section['name'], PressKits::POST_TYPE, array('priority' => 'high'));
			foreach($section['admin_fields'] as $id => $field) {
				$metabox->set_field($id, $field);
			}
		}
	}

	protected function enqueue_resources() {
		wp_enqueue_script(Plugin::slug('forms-colorpicker'), Plugin::url('js/lib/spectrum.js'), array('jquery'), Plugin::version());
		wp_enqueue_style(Plugin::slug('forms-colorpicker'), Plugin::url('css/lib/spectrum.css'), array(), Plugin::version());

		wp_enqueue_script(Plugin::slug('forms-datepicker'), Plugin::url('js/lib/bootstrap-datepicker.js'), array('jquery'), Plugin::version());
		wp_enqueue_style(Plugin::slug('forms-datepicker'), Plugin::url('css/lib/bootstrap-datepicker3.standalone.css'), array(), Plugin::version());

		wp_enqueue_script(Plugin::slug('forms-sortable'), Plugin::url('js/lib/jquery-sortable.js'), array('jquery'), Plugin::version());

		wp_enqueue_script(Plugin::slug('admin'), Plugin::url('js/admin/admin.js'), array('jquery', Plugin::slug('forms-colorpicker'), Plugin::slug('forms-datepicker')), Plugin::version());
		wp_enqueue_style(Plugin::slug('admin'), Plugin::url('css/admin/admin.css'), array(), Plugin::version());

		wp_enqueue_style(Plugin::slug('forms'), Plugin::url('css/admin/forms.css'), array(), Plugin::version());
	}

}

PressKits_Admin::add_action(Plugin::slug('init'), 'init');
