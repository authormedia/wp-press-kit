<?php

namespace WPPressKit;

final class PressKits_Public extends Singleton {

	protected function __construct() {}

	protected function init() {
		self::add_filter(PressKits::post_content_filter(), 'override_page_content', 100);
		self::add_action('wp_enqueue_scripts', 'enqueue_resources');
		self::setup_content_filter();
	}

	// used to filter the post content instead of 'the_content', to prevent things attached
	// to the global filter from interfering with page rendering (and to prevent an infinite
	// loop, as the PressKits page content override is attached to 'the_content')
	private function setup_content_filter() {
		$filter = Plugin::slug('content');
		add_filter($filter, 'wptexturize');
		add_filter($filter, 'convert_smilies');
		add_filter($filter, 'convert_chars');
		add_filter($filter, 'wpautop');
		add_filter($filter, 'capital_P_dangit');
		add_filter($filter, 'shortcode_unautop');
		add_filter($filter, 'do_shortcode');
	}

	protected function enqueue_resources() {
		if(PressKits::is_singular()) {
			wp_enqueue_style(Plugin::slug('public'), Plugin::url('css/public/public.css'), array(), Plugin::version());
			self::add_action('wp_head', 'add_dynamic_css');
		}
	}

	protected function add_dynamic_css() {
		$post = PressKits::get_post();
		/*$button_color = $post->get_meta('button_color');
		if($button_color) {
			echo('<style type="text/css">');
			echo('button { background-color: '.$button_color.' !important; } ');
			echo('</style>');
		}*/
	}

	protected function render_sections($post) {
		$output = '';

		$all_sections = Sections::get_sections();
		$sections = array_keys($all_sections);

		foreach($sections as $section) {
			if(!empty($all_sections[$section])) {
				$output .= apply_filters(Plugin::slug('render_'.$section.'_section'), call_user_func($all_sections[$section]['render'], $post), $post);
			}
		}

		return $output;
	}

	protected function override_page_content($content) {
		$post = PressKits::get_post();

		$output = '';
		$output .= '<div class="'.Plugin::slug('-content').'">';
 		$output .= self::render_sections($post);
		$output .= '</div>';

		return $output;
	}
}

PressKits_Public::add_action(Plugin::slug('init'), 'init');
