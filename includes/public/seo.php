<?php

namespace WPPressKit;

final class PressKits_SEO_Public extends Singleton {

	protected function __construct() {}

	protected function init() {
		if(defined('WPSEO_FILE')) {
			//WP SEO Integration
			self::add_action('wpseo_opengraph', 'add_wpseo_opengraph_image', 15);
		} else {
			//Custom SEO Overrides
			self::add_filter('wp_title', 'filter_wp_title', 999);
			self::add_filter('woo_title', 'filter_woo_title', 999, 3);
			self::add_action('wp_head', 'add_metadesc');
			self::add_action('wp_head', 'add_opengraph');
		}
	}



	/*---------------------------------------------------------*/
	/* WPSEO Integration Functions                             */
	/*---------------------------------------------------------*/

	protected function add_wpseo_opengraph_image() {
		if(PressKits::is_singular()) {
			$presskit = PressKits::get_post($GLOBALS['post']);
			$image = $presskit->get_meta('action_image');
		}

		if(!empty($image)) {
			echo('<meta property="og:image" content="'.esc_url($image).'"/>'.PHP_EOL);
		}
	}



	/*---------------------------------------------------------*/
	/* Custom SEO Overrides                                    */
	/*---------------------------------------------------------*/

	protected function seo_title($post_id = 0) {
		$title = '';

		if(!$post_id and isset($GLOBALS['post'])) { $post_id = $GLOBALS['post']; }
		$presskit = PressKits::get_post($post_id);
		if($presskit) {
			$seo_title = $presskit->get_meta(Plugin::slug('seo_title'));
			if(empty($seo_title)) {
				$title = get_the_title($presskit->ID);
			} else {
				$title = $seo_title;
			}
		}

		return $title;
	}

	protected function seo_tax_title($term = '', $taxonomy = '') {
		$title = '';

		$term_obj = null;
		if(empty($term) or empty($taxonomy)) {
			if(is_tax()) { $term_obj = get_queried_object(); }
		} else {
			$term_obj = get_term_by('slug', $term, $taxonomy);
		}

		if($term_obj and !empty($term_obj->labels)) {
			$title = $term_obj->labels->name.' - '.get_bloginfo('name');
		} else if($term_obj and !empty($term_obj->name)) {
			$title = $term_obj->name.' - '.get_bloginfo('name');
		}

		return $title;
	}

	protected function seo_metadesc($post_id = 0) {
		$metadesc = '';

		if(!$post_id and isset($GLOBALS['post'])) { $post_id = $GLOBALS['post']; }
		$presskit = PressKits::get_post($post_id);
		if($presskit) {
			$seo_metadesc = $presskit->get_meta(Plugin::slug('seo_metadesc'));
			if(empty($seo_metadesc)) {
				$metadesc = strip_tags($presskit->post_excerpt);
			} else {
				$metadesc = $seo_metadesc;
			}
		}

		return $metadesc;
	}

	protected function seo_tax_metadesc($term = '', $taxonomy = '') {
		$metadesc = '';

		$term_obj = null;
		if(empty($term) or empty($taxonomy)) {
			if(is_tax()) { $term_obj = get_queried_object(); }
		} else {
			$term_obj = get_term_by('slug', $term, $taxonomy);
		}

		if($term_obj and !empty($term_obj->description)) {
			$metadesc = $term_obj->description;
		}

		return $metadesc;
	}

	protected function seo_archive_title() {
		$labels = PressKits::get_option('labels');
		return $labels['name'].' - '.get_bloginfo('name');
	}

	protected function filter_wp_title($title) {
		$new_title = '';

		if(PressKits::is_singular()) {
			$new_title = $this->seo_title();
		} else if(PressKits::is_archive()) {
			$new_title = $this->seo_archive_title();
		}

		if(!empty($new_title)) {
			if(get_option('template') === 'twentyten' or get_option('template') === 'twentyeleven') { $title .= ' '; }
			if(substr($title, 0, 6) == '<title') { $new_title = '<title>'.$new_title.'</title>'; }
			$title = $new_title;
		}

		return $title;
	}

	protected function filter_woo_title($title, $sep, $raw_title) {
		if(PressKits::is_archive() or PressKits::is_singular()) {
			return $raw_title;
		}
		return $title;
	}

	protected function add_metadesc() {
		if(PressKits::is_singular()) {
			$metadesc = $this->seo_metadesc();
			if($metadesc) {
				echo('<meta name="description" content="'.htmlentities($metadesc).'"/>'.PHP_EOL);
			}
		}
	}

	protected function add_opengraph() {
		$tags = array();

		if(PressKits::is_singular()) {
			$tags['og:type'] = 'website';
			$tags['og:title'] = $this->seo_title();
			$tags['og:description'] = htmlentities($this->seo_metadesc());
			$tags['og:url'] = esc_url(get_permalink());
			$tags['og:site_name'] = get_bloginfo('name');
		} else if(PressKits::is_archive()) {
			$tags['og:type'] = 'website';
			$tags['og:title'] = $this->seo_archive_title();
			$tags['og:url'] = esc_url(get_archive_permalink());
			$tags['og:site_name'] = get_bloginfo('name');
		}

		foreach($tags as $tag => $content) {
			echo('<meta property="'.$tag.'" content="'.$content.'"/>'.PHP_EOL);
		}
	}
}

PressKits_SEO_Public::add_action(Plugin::slug('init'), 'init');
