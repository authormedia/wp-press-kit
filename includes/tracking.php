<?php

namespace WPPressKit;

final class Tracking extends Singleton {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	private $data = null;

	/*---------------------------------------------------------*/
	/* Protected Functions                                     */
	/*---------------------------------------------------------*/

	protected function __construct() {}

	protected function init() {
		Settings::add('allow_tracking');

		if(Settings::get('allow_tracking') === 'yes') {
			if(!wp_next_scheduled(Plugin::slug('periodic_tracking'))) { wp_schedule_event(time(), 'daily', Plugin::slug('periodic_tracking')); }
			self::add_action(Plugin::slug('periodic_tracking'), 'send_data');
		}
	}

	protected function track_event($name, $instance = false) {
		$this->load();

		$events = $this->data['events'];
		if(!isset($events[$name])) { $events[$name] = array(); }
		if(!isset($events[$name]['count'])) { $events[$name]['count'] = 0; }
		$events[$name]['count'] += 1;
		$events[$name]['last_time'] = time();

		if($instance !== false) {
			if(!is_array($instance)) { $instance = array(); }
			$instance['time'] = time();
			if(!isset($events[$name]['instances'])) { $events[$name]['instances'] = array(); }
			$events[$name]['instances'][] = $instance;
		}

		$this->data['events'] = $events;
		$this->save();
	}

	protected function send_data() {
		if(Settings::get('allow_tracking') !== 'yes') { return; }

		$data = array(
			'id' => $this->data['id'],
			'time' => time(),
			'version' => Plugin::version(),
			'events' => $this->data['events'],
		);

		$options = array(
			'timeout' => ((defined('DOING_CRON') && DOING_CRON) ? 30 : 3),
			'body' => array('data' => serialize($data)),
			'user-agent' => 'WordPress/'.$GLOBALS['wp_version'].'; '.get_bloginfo('url'),
		);

		$response = wp_remote_post('http://api.authormedia.com/plugins/'.Plugin::ID.'/analytics/submit', $options);
	}

	/*---------------------------------------------------------*/
	/* Private Functions                                       */
	/*---------------------------------------------------------*/

	private function load() {
		if($this->data !== null) { return; }

		$this->data = get_option(Plugin::slug('tracking_data'));
		if(empty($this->data)) {
			$id = hash('sha256', get_bloginfo('url').strval(time()));

			$this->data = array(
				'id' => $id,
				'events' => array(),
			);

			$this->save();
		}
	}

	private function save() {
		update_option(Plugin::slug('tracking_data'), $this->data);
	}
}

Tracking::add_action(Plugin::slug('init'), 'init');
