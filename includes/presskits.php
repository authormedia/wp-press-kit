<?php

namespace WPPressKit;

final class PressKits extends PostType {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	const POST_TYPE = 'presskit';

	/*---------------------------------------------------------*/
	/* Protected Functions                                     */
	/*---------------------------------------------------------*/

	protected function __construct() {
		parent::__construct();
	}

	protected function init() {
		parent::init();
		$this->generate_labels(__('Press Kit', Plugin::ID), __('Press Kits', Plugin::ID));
		$this->set_option('menu_icon', 'dashicons-megaphone');
		$this->set_option('has_archive', false);

		add_image_size(Plugin::slug('medium'), 1000, 400, false);
	}
}

PressKits::add_action(Plugin::slug('init'), 'init');

final class PressKit extends Post {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	const POST_TYPE = 'presskit';

	/*---------------------------------------------------------*/
	/* Public Functions                                        */
	/*---------------------------------------------------------*/

	public function __construct($post) {
		parent::__construct($post);
	}
}
