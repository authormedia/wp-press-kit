<?php

namespace WPPressKit;

final class Settings extends Singleton {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	private $data = null;
	private $settings = array();

	/*---------------------------------------------------------*/
	/* Protected Functions                                     */
	/*---------------------------------------------------------*/

	protected function __construct() {}

	protected function add($name, $options = null) {
		$options = wp_parse_args($options, array('default' => null));
		$this->settings[$name] = array('default' => $options['default']);
	}

	protected function get($name) {
		$this->load();
		if(!isset($this->settings[$name])) { throw new Exception('Unknown setting: '.$name); }
		return isset($this->data[$name]) ? $this->data[$name] : $this->settings[$name]['default'];
	}

	protected function set($name, $value) {
		$this->load();
		if(!isset($this->settings[$name])) { throw new Exception('Unknown setting: '.$name); }
		$this->data[$name] = $value;
		$this->save();
	}

	/*---------------------------------------------------------*/
	/* Private Functions                                       */
	/*---------------------------------------------------------*/

	private function load() {
		if($this->data !== null) { return; }
		$this->data = get_option(Plugin::slug('settings'));
	}

	private function save() {
		update_option(Plugin::slug('settings'), $this->data);
	}
}
