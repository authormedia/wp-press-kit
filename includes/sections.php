<?php

namespace WPPressKit;

final class Sections extends Singleton {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	private $sections = array();

	/*---------------------------------------------------------*/
	/* Protected Functions                                     */
	/*---------------------------------------------------------*/

	protected function __construct() {}

	protected function get_sections() {
		$sections = apply_filters(Plugin::slug('sections'), $this->sections);
		array_multisort(array_column($sections, 'priority'), SORT_ASC, $sections);
		return $sections;
	}

	protected function add_section($id, $section) {
		$this->sections[$id] = $section;
	}

	protected function remove_section($id) {
		unset($this->sections[$id]);
	}
}
//$default_sections = array('images', 'news', 'clippings', 'about', 'awards', 'twitter', 'downloads', 'mybooktable', 'myspeakingevents');

abstract class Section extends Singleton {

	protected function init() {
		if(!defined(get_called_class().'::ID')) { throw new Exception('Classes extending Section must define an ID constant'); }

		$section = array(
			'name' => static::name(),
			'priority' => static::priority(),
			'admin_fields' => static::admin_fields(),
			'render' => static::funcname('render'),
		);

		Sections::add_section(static::ID, $section);
	}

	abstract protected function name();
	protected function priority() { return 10; }
	abstract protected function admin_fields();
	abstract protected function render($post);

}
