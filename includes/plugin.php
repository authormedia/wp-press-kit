<?php

namespace WPPressKit;

require_once(dirname(__FILE__).'/lib/utils.php');

final class Plugin extends Singleton {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	const ID = 'wppresskit';
	const SLUG = 'wppresskit';
	const NAME = 'WP Press Kit';

	private $rootfile;
	private $version;
	private $basename;

	/*---------------------------------------------------------*/
	/* Getters/Setters                                         */
	/*---------------------------------------------------------*/

	protected function version() {
		return $this->version;
	}

	protected function basename() {
		return $this->basename;
	}

	/*---------------------------------------------------------*/
	/* Protected Functions                                     */
	/*---------------------------------------------------------*/

	protected function __construct() {
		$this->rootfile = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'wppresskit.php';
		$plugin_data = get_file_data($this->rootfile, array('version' => 'Version'));
		$this->version = $plugin_data['version'];
		$this->basename = plugin_basename($this->rootfile);
	}

	protected function init() {
		$this->includes();

		Settings::add('version', array('default' => Plugin::version()));

		self::add_action('activate_'.Plugin::basename(), 'do_activate_plugin');
		self::add_action('deactivate_'.Plugin::basename(), 'do_deactivate_plugin');

		self::add_filter('plugin_action_links_'.Plugin::basename(), 'filter_plugin_action_links');

		$this->detect_plugin_update();

		do_action(Plugin::slug('init'));
	}

	protected function filter_plugin_action_links($actions) {
		unset($actions['edit']);
		$actions['review'] = '<a target="_blank" href="http://wordpress.org/support/view/plugin-reviews/'.Plugin::SLUG.'?filter=5#postform">'.__('Write a Review', Plugin::ID).'</a>';
		return $actions;
	}

	protected function do_activate_plugin() {
		PressKits::register();
		flush_rewrite_rules();
	}

	protected function do_deactivate_plugin() {
		flush_rewrite_rules();
	}

	protected function path($path) {
		return dirname($this->rootfile).DIRECTORY_SEPARATOR.$path;
	}

	protected function url($url) {
		return plugins_url($url, $this->rootfile);
	}

	protected function slug($identifier) {
		$delimeter = strpos($identifier, '-') === false ? '_' : '-';
		if($identifier[0] === $delimeter) { $identifier = substr($identifier, 1); }
		return Plugin::SLUG.$delimeter.$identifier;
	}

	/*---------------------------------------------------------*/
	/* Private Functions                                       */
	/*---------------------------------------------------------*/

	private function includes() {
		require_once(Plugin::path('includes/lib/forms.php'));
		require_once(Plugin::path('includes/settings.php'));
		require_once(Plugin::path('includes/presskits.php'));
		require_once(Plugin::path('includes/sections.php'));
		require_once(Plugin::path('includes/tracking.php'));
		require_all(Plugin::path('includes/sections'));

		if(is_admin()) {
			require_once(Plugin::path('includes/admin/presskits.php'));
			require_once(Plugin::path('includes/admin/notices.php'));
			require_once(Plugin::path('includes/admin/seo.php'));
		} else {
			require_once(Plugin::path('includes/public/presskits.php'));
			require_once(Plugin::path('includes/public/seo.php'));
		}
	}

	private function detect_plugin_update() {
		Settings::set('version', Plugin::version());
	}
}

Plugin::add_action('plugins_loaded', 'init');
