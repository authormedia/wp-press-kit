<?php

namespace WPPressKit\Forms;
use WPPressKit\Plugin as Context;

use function WPPressKit\add_ajax_action;
use function WPPressKit\post_get_int;
use function WPPressKit\post_get_str;
use function WPPressKit\post_get_json;
use function WPPressKit\wp_slash_fixed;

class Exception extends \Exception {}

// do $data->store(save($field, $id)) instead of save($field, $id, $data)
// change the basic form field pattern to a field set that makes a table

/*---------------------------------------------------------*/
/* Main Classes                                            */
/*---------------------------------------------------------*/

class FormFields {
	private static $fieldtypes;

	private function __construct() {}

	public static function get_types() {
		return self::$fieldtypes;
	}

	public static function register_type($type, $fieldtype) {
		if(isset($fields[$type])) { throw new Exception('Field type "'.$type.'" already defined'); }
		if(empty($fieldtype['name'])) { throw new Exception('Fields registered with FormFields must include a name value'); }
		if(empty($fieldtype['render'])) { throw new Exception('Fields registered with FormFields must include a render function'); }
		if(empty($fieldtype['save'])) { throw new Exception('Fields registered with FormFields must include a save function'); }
		if(empty($fieldtype['properties'])) { $fieldtype['properties'] = array(); }
		self::$fieldtypes[$type] = $fieldtype;
	}

	public static function render($field, $id, $data) {
		assert(is_array($field), 'Field must be an array');
		assert(isset($field['type']), 'Fields must have a valid "type" value');
		if(!isset(self::$fieldtypes[$field['type']])) { throw new Exception('Undefined field type "'.$field['type'].'"'); }
		$output = call_user_func_array(self::$fieldtypes[$field['type']]['render'], array($field, $id, $data));
		return apply_filters(Context::slug('render_form_field_'.$field['type']), $output, $field, $id, $data);
	}

	public static function save($field, $id, $data) {
		assert(is_array($field), 'Field must be an array');
		assert(isset($field['type']), 'Fields must have a valid "type" value');
		if(!isset(self::$fieldtypes[$field['type']])) { throw new Exception('Undefined field type "'.$field['type'].'"'); }
		call_user_func_array(self::$fieldtypes[$field['type']]['save'], array($field, $id, $data));
		do_action(Context::slug('save_form_field_'.$field['type']), $field, $id, $data);
	}
}

/*---------------------------------------------------------*/
/* Property Types                                          */
/*---------------------------------------------------------*/

class FormFieldProperties {
	private static $proptypes;

	private function __construct() {}

	public static function get_types() {
		return self::$proptypes;
	}

	public static function register_type($type, $validate_function) {
		self::$proptypes[$type] = array('validate' => $validate_function);
	}

	public static function validate($field) {
		assert(is_array($field), 'Field must be an array');
		assert(isset($field['type']), 'Fields must have a valid "type" value');
		$fieldtypes = FormFields::get_types();
		if(!isset($fieldtypes[$field['type']])) { throw new Exception('Undefined field type "'.$field['type'].'"'); }

		$fieldtype = $fieldtypes[$field['type']];
		$properties = apply_filters(Context::slug('form_field_properties'), $fieldtype['properties'], $field);

		assert(is_array($properties), 'Field properties list must be an array');
		foreach($properties as $prop_name => $property) {
			assert(is_string($prop_name), 'Field properties list must be an associative array');
			assert(is_array($property), 'Field properties must be arrays');
			if(!isset($property['type']) or !is_string($property['type'])) { throw new Exception('Field properties must include a valid "type" value'); }
			if(!isset(self::$proptypes[$property['type']])) { throw new Exception('Undefined property type "'.$property['type'].'"'); }
			if(!isset($property['name']) or !is_string($property['name'])) { throw new Exception('Field properties must include a valid "name" value'); }
			if(isset($property['required']) and $property['required'] === true and !isset($field[$prop_name])) { throw new Exception('Required property "'.$property['name'].'" missing from field "'.$fieldtype['name'].'"'); }
			if(isset($property['default']) and !isset($field[$prop_name])) { $field[$prop_name] = $property['default']; }
		}

		foreach($field as $prop_name => $prop_value) {
			if($prop_name == 'type') { continue; }
			if(!isset($properties[$prop_name])) { unset($field[$prop_name]); continue; }
			$prop_value = call_user_func_array(self::$proptypes[$properties[$prop_name]['type']]['validate'], array($prop_value, $properties[$prop_name], $field));
			$field[$prop_name] = apply_filters(Context::slug('validate_form_field_property'), $prop_value, $properties[$prop_name], $field);
		}

		return apply_filters(Context::slug('validate_form_field_properties'), $field);
	}
}

function validate_any_property($value, $property, $fieldtype) {	return $value; }
FormFieldProperties::register_type('any', __NAMESPACE__.'\\'.'validate_any_property');

function validate_number_property($value, $property, $fieldtype) {
	if(!is_numeric($value)) { throw new Exception('Field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "number" must be numeric'); }
	return $value;
}
FormFieldProperties::register_type('number', __NAMESPACE__.'\\'.'validate_number_property');

function validate_text_property($value, $property, $fieldtype) {
	if(!is_string($value)) { throw new Exception('Field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "text" must be a string'); }
	return $value;
}
FormFieldProperties::register_type('text', __NAMESPACE__.'\\'.'validate_text_property');

function validate_checkbox_property($value, $property, $fieldtype) {
	if(!is_bool($value)) { throw new Exception('Field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "checkbox" must be a boolean'); }
	return $value;
}
FormFieldProperties::register_type('checkbox', __NAMESPACE__.'\\'.'validate_checkbox_property');

function validate_options_property($value, $property, $fieldtype) {
	$associative_options = array_keys($property['options']) !== range(0, count($property['options']) - 1);
	if($associative_options) {
		if(!in_array($value, array_keys($property['options']))) { throw new Exception('The value "'.strval($value).'" for field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "options" is not a valid option'); }
	} else {
		if(!in_array($value, $property['options'])) { throw new Exception('The value "'.strval($value).'" for field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "options" is not a valid option'); }
	}
	return $value;
}
FormFieldProperties::register_type('options', __NAMESPACE__.'\\'.'validate_options_property');

function validate_field_property($value, $property, $fieldtype) {
	return FormFieldProperties::validate($value);
}
FormFieldProperties::register_type('field', __NAMESPACE__.'\\'.'validate_field_property');

function validate_fields_property($value, $property, $fieldtype) {
	if(!is_array($value)) { throw new Exception('Field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "fields" must be an array'); }
	foreach($value as $id => $subfield) {
		if(!is_string($id)) { throw new Exception('Field property "'.$property['name'].'" of field "'.$fieldtype['name'].'" of type "fields" must be an associative array'); }
		$value[$id] = FormFieldProperties::validate($subfield);
	}
	return $value;
}
FormFieldProperties::register_type('fields', __NAMESPACE__.'\\'.'validate_fields_property');

/*---------------------------------------------------------*/
/* Form Data Classes                                       */
/*---------------------------------------------------------*/

abstract class FormFieldData {
	abstract public function retrieve();
	abstract public function store($data);
}

class EmptyFormFieldData extends FormFieldData {
	public function retrieve() { return null; }
	public function store($data) {}
}

class ChildFormFieldData extends FormFieldData {
	protected $id;
	protected $parent;

	public function __construct($id, $parent) {
		if(!$parent instanceof FormFieldData) { throw new Exception('The provided parent object must implement FormFieldData'); }
		$this->id = $id;
		$this->parent = $parent;
	}

	public function retrieve() {
		$parent_data = $this->parent->retrieve();
		if($parent_data == null) { return null; }
		if(!is_array($parent_data)) { throw new Exception('The parent object of a ChildFormFieldData must store an array'); }
		return isset($parent_data[$this->id]) ? $parent_data[$this->id] : null;
	}

	public function store($data) {
		$parent_data = $this->parent->retrieve();
		if($parent_data == null) { $parent_data = array(); }
		if(!is_array($parent_data)) { throw new Exception('The parent object of a ChildFormFieldData must store an array'); }
		$parent_data[$this->id] = $data;
		$this->parent->store($parent_data);
	}
}

/*---------------------------------------------------------*/
/* Form Field Classes                                     */
/*---------------------------------------------------------*/

abstract class FormField {
	public static function register() {
		$class = get_called_class();
		if(!defined($class.'::FIELD_TYPE')) { throw new Exception('Classes extending FormField must define the FIELD_TYPE constant'); }
		if(!defined($class.'::FIELD_NAME')) { throw new Exception('Classes extending FormField must define the FIELD_NAME constant'); }
		$instance = new $class;
		FormFields::register_type(static::FIELD_TYPE, array(
			'name' => static::FIELD_NAME,
			'properties' => $instance->properties(),
			'render' => array($instance, 'render'),
			'save' => array($instance, 'save'),
		));
		return $instance;
	}

	public function properties() { return array(); }
	abstract public function render($field, $id, $data);
	abstract public function save($field, $id, $data);
}

abstract class BasicFormField extends FormField {
	public function properties() {
		$properties = parent::properties();
		$properties['name'] = array('type' => 'text', 'name' => __('Field Name', Context::ID), 'required' => true);
		$properties['desc'] = array('type' => 'text', 'name' => __('Field Description', Context::ID), 'default' => '');
		return $properties;
	}

	public function render($field, $id, $data) {
		$output = '';
		$output .= '<div class="'.$this->render_class($field, $id, $data).'">';
		$output .= '<div class="'.Context::slug('field-title').'">'.str_replace('_', '-', $field['name']).':</div>';
		$output .= '<div class="'.Context::slug('field-content').'">';
		$output .= $this->render_content($field, $id, $data);
		$output .= '</div>';
		if($field['desc']) {
			$output .= '<div class="'.Context::slug('field-desc').'">'.$field['desc'].'</div>';
		}
		$output .= '</div>';
		return $output;
	}

	protected function render_class($field, $id, $data) {
		return Context::slug('-field').' '.Context::slug(static::FIELD_TYPE.'-field');
	}

	abstract protected function render_content($field, $id, $data);
}

class NumberFormField extends BasicFormField {
	const FIELD_TYPE = 'number';
	const FIELD_NAME = 'Number';

	public function properties() {
		$properties = parent::properties();
		$properties['default'] = array('type' => 'number', 'name' => __('Default Value', Context::ID), 'default' => 0);
		$properties['integral'] = array('type' => 'checkbox', 'name' => __('Integral', Context::ID), 'desc' => __('Only allow whole numbers to be selected', Context::ID), 'default' => false);
		return $properties;
	}

	protected function render_content($field, $id, $data) {
		$value = $data->retrieve();
		if(!is_numeric($value)) { $value = $field['default']; }
		$input_classes = Context::slug('number-field-input').($field['integral'] ? Context::slug('number-field-integral') : '');
		$output = '';
		$output .= '<div class="'.Context::slug('number-field-input-container').'">';
		$output .= '<input type="number" class="'.$input_classes.'" name="'.$id.'" value="'.$value.'">';
		$output .= '</div>';
		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_str($id);
		if($value === null or !is_numeric($value)) { throw new Exception('Unable to save number field'); }
		$data->store($field['integral'] ? intval($value) : floatval($value));
	}
}
NumberFormField::register();

class TextFormField extends BasicFormField {
	const FIELD_TYPE = 'text';
	const FIELD_NAME = 'Text';

	public function properties() {
		$properties = parent::properties();
		$properties['default'] = array('type' => 'text', 'name' => __('Default Value', Context::ID), 'default' => '');
		$properties['pattern'] = array('type' => 'text', 'name' => __('Pattern', Context::ID), 'desc' => __('Regex pattern used for validation', Context::ID), 'default' => '');
		return $properties;
	}

	protected function render_content($field, $id, $data) {
		$value = $data->retrieve();
		if(!is_string($value)) { $value = $field['default']; }
		$output = '';
		$output .= '<div class="'.Context::slug('text-field-input-container').'">';
		$output .= '<input type="text" class="'.Context::slug('text-field-input').'" name="'.$id.'"'.($field['pattern'] ? ' pattern="'.$field['pattern'].'"' : '').' value="'.htmlspecialchars($value, ENT_QUOTES).'">';
		$output .= '</div>';
		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_str($id);
		if($value === null) { throw new Exception('Unable to save text field'); }
		$data->store($value);
	}
}
TextFormField::register();

class TextareaFormField extends BasicFormField {
	const FIELD_TYPE = 'textarea';
	const FIELD_NAME = 'Textarea';

	public function properties() {
		$properties = parent::properties();
		$properties['default'] = array('type' => 'text', 'name' => __('Default Value', Context::ID), 'default' => '');
		return $properties;
	}

	protected function render_content($field, $id, $data) {
		$value = $data->retrieve();
		if(!is_string($value)) { $value = $field['default']; }
		$output = '';
		$output .= '<div class="'.Context::slug('textarea-field-input-container').'">';
		$output .= '<textarea class="'.Context::slug('textarea-field-input').'" name="'.$id.'">'.htmlspecialchars($value, ENT_QUOTES).'</textarea>';
		$output .= '</div>';
		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_str($id);
		if($value === null) { throw new Exception('Unable to save textarea field'); }
		$data->store($value);
	}
}
TextareaFormField::register();

class WysiwygFormField extends BasicFormField {
	const FIELD_TYPE = 'wysiwyg';
	const FIELD_NAME = 'Wysiwyg';

	public function properties() {
		$properties = parent::properties();
		$properties['default'] = array('type' => 'text', 'name' => __('Default Value', Context::ID), 'default' => '');
		return $properties;
	}

	protected function render_content($field, $id, $data) {
		$value = $data->retrieve();
		if(!is_string($value)) { $value = $field['default']; }

		ob_start();
		wp_editor($value, $id);
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_str($id);
		if($value === null) { throw new Exception('Unable to save wysiwyg field'); }
		$data->store($value);
	}
}
WysiwygFormField::register();

class FileFormField extends BasicFormField {
	const FIELD_TYPE = 'file';
	const FIELD_NAME = 'File';

	public function properties() {
		$properties = parent::properties();
		$properties['labels'] = array('type' => 'any', 'name' => __('Labels', Context::ID), 'default' => array('choose' => __('Choose', Context::ID), 'clear' => __('Clear', Context::ID))); // TODO: Create appropriate property type
		return $properties;
	}

	protected function render_content($field, $id, $data) {
		$attachment_id = $data->retrieve();
		$attachment_id = !is_integer($attachment_id) ? null : intval($attachment_id);

		$output = '';
		$output .= '<div class="'.Context::slug('file-selector').'">';
		$output .= '	<input type="hidden" class="'.Context::slug('file-selector-input').'" name="'.$id.'" value="'.($attachment_id ? $attachment_id : '').'">';
		$output .= '	<div class="'.Context::slug('file-selector-preview').'">';
		if($attachment_id) {
			$output .= '<div class="'.Context::slug('file-selector-preview-image').'"><img src="'.wp_get_attachment_image_src($attachment_id, 'full', true)[0].'"></div>';
			$output .= '<div class="'.Context::slug('file-selector-preview-text').'">'.basename(get_attached_file($attachment_id)).'</div>';
		}
		$output .= '	</div>';
		$output .= '	<div class="'.Context::slug('file-selector-actions').'">';
		$output .= '		<div class="'.Context::slug('file-selector-select').'">'.$field['labels']['choose'].'</div>';
		$output .= '		<div class="'.Context::slug('file-selector-clear').'">'.$field['labels']['clear'].'</div>';
		$output .= '	</div>';
		$output .= '</div>';

		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_int($id);
		$data->store($value);
	}
}
FileFormField::register();

class FileURLFormField extends BasicFormField {
	const FIELD_TYPE = 'file_url';
	const FIELD_NAME = 'File URL';

	public function properties() {
		$properties = parent::properties();
		$properties['default'] = array('type' => 'text', 'name' => __('Default Value', Context::ID), 'default' => '');
		$properties['labels'] = array('type' => 'any', 'name' => __('Labels', Context::ID), 'default' => array('choose' => __('Choose', Context::ID))); // TODO: Create appropriate property type
		return $properties;
	}

	protected function render_content($field, $id, $data) {
		$url = $data->retrieve();
		if(!is_string($url)) { $url = $field['default']; }

		$output = '';
		$output .= '<div class="'.Context::slug('file-url-field-input-container').'">';
		$output .= '	<input type="text" class="'.Context::slug('file-url-field-input').'" name="'.$id.'" value="'.htmlspecialchars($url, ENT_QUOTES).'">';
		$output .= '</div>';
		$output .= '<div class="button '.Context::slug('file-url-field-select').'">'.$field['labels']['choose'].'</div>';

		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_str($id);
		if($value === null) { throw new Exception('Unable to save file url field'); }
		$data->store($value);
	}
}
FileURLFormField::register();

class DateFormField extends BasicFormField {
	const FIELD_TYPE = 'date';
	const FIELD_NAME = 'Date';

	protected function render_content($field, $id, $data) {
		$date = $data->retrieve();
		$date_formatted = '';

		if(!is_integer($date)) { $date = ''; }
		if(!empty($date)) { $date_formatted = \date_format(\date_create('@'.$date, \timezone_open('UTC')), 'n/j/Y'); }

		$output = '';
		$output .= '<div class="'.Context::slug('date-field-container').'">';
		$output .= '	<input type="hidden" class="'.Context::slug('date-field-input').'" name="'.$id.'" value="'.$date.'">';
		$output .= '	<input type="text" class="'.Context::slug('date-field-selector').'" value="'.$date_formatted.'">';
		$output .= '</div>';

		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_int($id);
		$data->store($value);
	}
}
DateFormField::register();

class TaxonomyTermFormField extends BasicFormField {
	const FIELD_TYPE = 'taxonomy_term';
	const FIELD_NAME = 'Taxonomy & Term';

	public static function register() {
		$instance = parent::register();
		add_ajax_action(Context::slug('taxonomy_term_form_field_get_terms'), array($instance, 'ajax_get_terms'));
		return $instance;
	}

	protected function render_content($field, $id, $data) {
		$data = $data->retrieve();
		if(!is_array($data) or empty($data['taxonomy']) or empty($data['term'])) { $data = null; }

		$output = '';
		$output .= '<input type="hidden" class="'.Context::slug('taxonomy-term-field-input').'" name="'.$id.'" value="'.($data ? esc_attr(json_encode($data)) : '').'">';

		$taxonomies = get_taxonomies(array('object_type' => array('post'), 'show_ui' => true), 'objects');
		$selected_tax = null;
		if(is_array($taxonomies) and !empty($taxonomies)) {
			$output .= '<select class="'.Context::slug('taxonomy-term-field-taxonomy').'">';
			$output .= '<option value="">'.__('-- Choose One --', Context::ID).'</option>';
			foreach($taxonomies as $taxonomy) {
				$selected = $data and $taxonomy->name == $data['taxonomy'];
				if($selected) { $selected_tax = $taxonomy->name; }
				$output .= '<option value="'.$taxonomy->name.'"'.($selected ? ' selected' : '').'>'.$taxonomy->labels->name.'</option>';
			}
			$output .= '</select>';
		} else {
			$output .= '<select class="'.Context::slug('taxonomy-term-field-taxonomy').'" disabled>';
			$output .= '<option value="">'.__('No Taxonomies Available', Context::ID).'</option>';
			$output .= '</select>';
		}

		if($selected_tax) {
			$terms = get_terms(array('taxonomy' => $selected_tax));
			if(is_array($terms) and !empty($terms)) {
				$output .= '<select class="'.Context::slug('taxonomy-term-field-term').'">';
				$output .= '<option value="">'.__('-- Choose One --', Context::ID).'</option>';
				foreach($terms as $term) {
					$selected = $data and $term->name == $data['term'];
					$output .= '<option value="'.$term->name.'"'.($selected ? ' selected' : '').'>'.$term->name.'</option>';
				}
				$output .= '</select>';
			} else {
				$output .= '<select class="'.Context::slug('taxonomy-term-field-term').'" disabled>';
				$output .= '<option value="">'.__('No Terms Available', Context::ID).'</option>';
				$output .= '</select>';
			}
		} else {
			$output .= '<select class="'.Context::slug('taxonomy-term-field-term').'" disabled>';
			$output .= '<option value="">'.__('Choose Taxonomy', Context::ID).'</option>';
			$output .= '</select>';
		}

		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_json($id);
		if(!is_array($value) or empty($value['taxonomy']) or empty($value['term'])) { $value = null; }
		$data->store($value);
	}

	public function ajax_get_terms() {
		$taxonomy = post_get_str('taxonomy');
		if($taxonomy === null) { throw new Exception('Unable to complete taxonomy/term ajax request'); }

		$output = '';
		if($taxonomy) {
			$terms = get_terms(array('taxonomy' => $taxonomy));
			if(is_array($terms) and !empty($terms)) {
				$output .= '<select class="'.Context::slug('taxonomy-term-field-term').'">';
				$output .= '<option value="">'.__('-- Choose One --', Context::ID).'</option>';
				foreach($terms as $term) {
					$selected = $data and $term->name == $data['term'];
					$output .= '<option value="'.$term->name.'"'.($selected ? ' selected' : '').'>'.$term->name.'</option>';
				}
				$output .= '</select>';
			} else {
				$output .= '<select class="'.Context::slug('taxonomy-term-field-term').'" disabled>';
				$output .= '<option value="">'.__('No Terms Available', Context::ID).'</option>';
				$output .= '</select>';
			}
		} else {
			$output .= '<select class="'.Context::slug('taxonomy-term-field-term').'" disabled>';
			$output .= '<option value="">'.__('Choose Taxonomy', Context::ID).'</option>';
			$output .= '</select>';
		}
		echo($output);

		die();
	}
}
TaxonomyTermFormField::register();

class SorterFormField extends FormField {
	const FIELD_TYPE = 'sorter';
	const FIELD_NAME = 'Sorter';

	public function properties() {
		$properties = parent::properties();
		$properties['name'] = array('type' => 'text', 'name' => __('Field Name', Context::ID), 'default' => '');
		$properties['items'] = array('type' => 'any', 'name' => __('Items', Context::ID));
		return $properties;
	}

	protected function get_sorted_items($field, $id, $data) {

		/*$subfield_priorities = array();
		foreach($field['fields'] as $id => $subfield) {
			if(!isset($subfield['priority'])) { $subfield['priority'] = 10; }
			if(!isset($subfield_priorities[$subfield['priority']])) { $subfield_priorities[$subfield['priority']] = array(); }
			$subfield_priorities[$subfield['priority']][] = array('id' => $id, 'field' => $subfield);
		}
		$prioritized_subfields = array();
		foreach($subfield_priorities as $priority => $subfields) {
			foreach($subfields as $subfield) {
				$prioritized_subfields[] = $subfield;
			}
		}
		return $prioritized_subfields;

		$sections = apply_filters('mbt_get_'.$display_mode.'_content_sections', array());
		$prioritized_sections = array();
		foreach($sections as $id => $section) { $prioritized_sections[] = array_merge($section, array('id' => $id)); }
		$prioritize = function($a, $b) { return ($a['priority'] == $b['priority']) ? 0 : (($a['priority'] < $b['priority']) ? -1 : 1); };
		usort($prioritized_sections, $prioritize);
		$sections_order = mbt_get_setting('book_section_order_'.$display_mode);
		if(empty($sections_order)) { $sections_order = array(); }

		$sorted = array();
		$order_index = 0;

		foreach($prioritized_sections as $section) {
			$index = array_search($section['id'], $sections_order);
			if($index === false) {
				$sorted[] = $section;
			} else {
				while(empty($sections[$sections_order[$order_index]])) { $order_index += 1; }
				$order_section = $sections[$sections_order[$order_index]];
				$sorted[] = array_merge($order_section, array('id' => $sections_order[$order_index], 'priority' => $section['priority']));
				$order_index += 1;
			}
		}*/

		// TODO: Implement

		$sorted = $field['options'];

		return $sorted;
	}

	public function render($field, $id, $data) {
		$order = $data->retrieve();

		$output = '';
		$output .= '<div class="'.$this->render_class($field, $id, $data).'">';
		if(!empty($field['name'])) { $output .= '<div class="'.Context::slug('sorter-title').'">'.$field['name'].'</div>'; }
		$output .= '<input type="hidden" class="'.Context::slug('sorter-input').'" name="'.$id.'" value="'.($order ? esc_attr(json_encode($order)) : '').'">';
		$output .= $this->render_items($field, $id, $data);
		$output .= '</div>';

		return $output;
	}

	protected function render_class($field, $id, $data) {
		return Context::slug('-group').' '.$direction_class;
	}

	protected function render_items($field, $id, $data) {
		$output = '';
		$items = $this->get_sorted_items($field, $id, $data);
		if(!empty($items)) {
			$output .= '<ul class="'.Context::slug('sorter-list').'">';
			foreach($items as $id => $name) {
				$output .= '<li class="'.Context::slug('sorter-item').'" data-'.Context::slug('sorter-item-id').'="'.esc_attr($id).'">'.htmlspecialchars($name, ENT_QUOTES).'</li>';
			}
			$output .= '</ul>';
		}
		return $output;
	}

	public function save($field, $id, $data) {
		$value = post_get_json($id);
		$data->store($value);
	}
}
SorterFormField::register();

class GroupFormField extends FormField {
	const FIELD_TYPE = 'group';
	const FIELD_NAME = 'Group';

	public function properties() {
		$properties = parent::properties();
		$properties['name'] = array('type' => 'text', 'name' => __('Group Name', Context::ID), 'default' => '');
		$properties['direction'] = array('type' => 'options', 'name' => __('Layout Direction', Context::ID), 'options' => array('vertical' => __('Vertical', Context::ID), 'horizontal' => __('Horizontal', Context::ID)), 'default' => 'vertical');
		$properties['fields'] = array('type' => 'fields', 'name' => __('Fields', Context::ID), 'default' => array());
		return $properties;
	}

	protected function get_prioritized_subfields($field) {
		$subfield_priorities = array();
		foreach($field['fields'] as $id => $subfield) {
			if(!isset($subfield['priority'])) { $subfield['priority'] = 10; }
			if(!isset($subfield_priorities[$subfield['priority']])) { $subfield_priorities[$subfield['priority']] = array(); }
			$subfield_priorities[$subfield['priority']][] = array('id' => $id, 'field' => $subfield);
		}
		$prioritized_subfields = array();
		foreach($subfield_priorities as $priority => $subfields) {
			foreach($subfields as $subfield) {
				$prioritized_subfields[] = $subfield;
			}
		}
		return $prioritized_subfields;
	}

	public function render($field, $id, $data) {
		$output = '';
		$output .= '<div class="'.$this->render_class($field, $id, $data).'">';
		if(!empty($field['name'])) { $output .= '<div class="'.Context::slug('group-title').'">'.$field['name'].'</div>'; }
		$output .= $this->render_subfields($field, $id, $data);
		$output .= '</div>';
		return $output;
	}

	protected function render_class($field, $id, $data) {
		$direction_class = $field['direction'] == 'horizontal' ? Context::slug('group-horizontal') : Context::slug('group-vertical');
		return Context::slug('-group').' '.$direction_class;
	}

	protected function render_subfields($field, $id, $data) {
		$output = '';
		$subfields = $this->get_prioritized_subfields($field);
		if(!empty($subfields)) {
			$output .= '<ul class="'.Context::slug('group-list').'">';
			foreach($subfields as $subfield) {
				$output .= '<li class="'.Context::slug('group-item').'">';
				$output .= FormFields::render($subfield['field'], $id.'_'.$subfield['id'], new ChildFormFieldData($subfield['id'], $data));
				$output .= '</li>';
			}
			$output .= '</ul>';
		}
		return $output;
	}

	public function save($field, $id, $data) {
		foreach($field['fields'] as $subfield_id => $subfield) {
			FormFields::save($subfield, $id.'_'.$subfield_id, new ChildFormFieldData($subfield_id, $data));
		}
	}
}
GroupFormField::register();

class RepeaterFormField extends FormField {
	const FIELD_TYPE = 'repeater';
	const FIELD_NAME = 'Repeater';

	public static function register() {
		$instance = parent::register();
		add_ajax_action(Context::slug('repeater_form_field_add_item'), array($instance, 'ajax_add_item'));
		return $instance;
	}

	public function properties() {
		$properties = parent::properties();
		$properties['name'] = array('type' => 'text', 'name' => __('Group Name', Context::ID), 'default' => '');
		$properties['labels'] = array('type' => 'any', 'name' => __('Labels', Context::ID), 'default' => array('add_new' => __('Add New', Context::ID))); // TODO: Create appropriate property type
		$properties['field'] = array('type' => 'field', 'name' => __('Field', Context::ID), 'default' => array());
		$properties['desc'] = array('type' => 'text', 'name' => __('Field Description', Context::ID), 'default' => '');
		return $properties;
	}

	public function render($field, $id, $data) {
		$output = '';
		$items = $data->retrieve();
		$count = is_array($items) ? count($items) : 0;
		$output .= '<div class="'.$this->render_class($field, $id, $data).'">';
		$output .= '<script type="text/json" class="'.Context::slug('repeater-data').'">'.json_encode(array('field' => $field, 'id' => $id)).'</script>';
		if(!empty($field['name'])) { $output .= '<div class="'.Context::slug('repeater-title').'">'.$field['name'].'</div>'; }
		$output .= '<div class="button '.Context::slug('repeater-add-item').'">'.$field['labels']['add_new'].'</div>';
		$output .= '<input type="hidden" class="'.Context::slug('repeater-count').'" name="'.$id.'_count" value="'.$count.'">';
		$output .= '<ul class="'.Context::slug('repeater-list').'">';
		for($i = 0; $i < count($items); $i++) {
			$output .= $this->render_subfield($field, $id, $i, $data);
		}
		$output .= '</ul>';
		if($field['desc']) {
			$output .= '<div class="'.Context::slug('field-desc').'">'.$field['desc'].'</div>';
		}
		$output .= '</div>';
		return $output;
	}

	protected function render_class($field, $id, $data) {
		return Context::slug('-field').' '.Context::slug('-repeater');
	}

	public function save($field, $id, $data) {
		$count = post_get_int($id.'_count');
		if($count === null) { throw new Exception('Unable to save repeater field'); }
		$data->store(null);
		for($i = 0; $i < $count; $i++) {
			FormFields::save($field['field'], $id.'_'.$i, new ChildFormFieldData($i, $data));
		}
	}

	public function validate($field) {
		$field = parent::validate($field);
		$field['field'] = FormFields::validate($field['field']);
		$singular_name = isset($field['labels']['singular_name']) ? $field['labels']['singular_name'] : null;
		$plural_name = isset($field['labels']['plural_name']) ? $field['labels']['plural_name'] : null;
		if(!isset($field['labels']['add_new'])) { $field['labels']['add_new'] = $singular_name ? sprintf(__('Add New %s', Context::ID), $singular_name) : __('Add New', Context::ID); }
		return $field;
	}

	public function render_subfield($field, $id, $index, $data) {
		$output = '';
		$output .= '<li class="'.Context::slug('repeater-item').'">';
		$output .= '<div class="'.Context::slug('repeater-item-remove').'"></div>';
		$output .= '<div class="'.Context::slug('repeater-item-handle').'"></div>';
		$output .= FormFields::render($field['field'], $id.'_'.$index, new ChildFormFieldData($index, $data));
		$output .= '</li>';
		return $output;
	}

	public function ajax_add_item() {
		$field = post_get_json('field');
		$id = post_get_str('id');
		if($field === null or $id === null) { throw new Exception('Unable to complete repeater ajax request'); }
		echo($this->render_subfield($field, $id, 0, new EmptyFormFieldData()));
		die();
	}
}
RepeaterFormField::register();

/*---------------------------------------------------------*/
/* Post Metabox Classes                                    */
/*---------------------------------------------------------*/

class PostMetaFormFieldData extends FormFieldData {
	protected $post_id;
	protected $id;

	public function __construct($post_id, $id) {
		$this->post_id = $post_id;
		$this->id = $id;
	}

	public function retrieve() {
		return get_post_meta($this->post_id, $this->id, true);
	}

	public function store($data) {
		// Needs a wp_slash because update_post_meta just straight up wp_unslashes your data for no reason
		// Needs to be fixed because otherwise it eats NULLs
		update_post_meta($this->post_id, $this->id, wp_slash_fixed($data));
	}
}

final class PostMetabox {
	private $id;
	private $name;
	private $post_type;
	private $options;
	private $fields;

	public function __construct($id, $name, $post_type, $options = null) {
		$options = wp_parse_args($options, array(
			'context' => 'normal',
			'priority' => 'default',
		));

		$this->id = $id;
		$this->name = $name;
		$this->post_type = $post_type;
		$this->options = $options;

		add_action('add_meta_boxes', array($this, 'add_meta_box'));
		add_action('save_post', array($this, 'do_save_post'));
	}

	public function set_field($id, $field) {
		$this->fields[$id] = $field;
	}

	public function remove_field($id) {
		unset($this->fields[$id]);
	}

	public function get_fields() {
		return apply_filters(Context::slug($this->id.'_metabox_fields'), $this->fields);
	}

	public function add_meta_box() {
		add_meta_box(Context::slug($this->id.'_metabox'), $this->name, array($this, 'render'), $this->post_type, $this->options['context'], $this->options['priority']);
	}

	public function do_save_post($post_id) {
		if((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || get_post_status($post_id) == 'auto-draft' || get_post_type($post_id) !== $this->post_type) { return; }
		$this->save($post_id);
	}

	public function render($post_id) {
		if($post_id instanceof \WP_Post) { $post_id = $post_id->ID; }
		$output = '';
		$output .= '<div class="'.Context::slug('form-fields').' '.Context::slug('-metabox').' '.Context::slug($this->id.'-metabox').'">';
		$field = FormFieldProperties::validate(array('type' => 'group', 'fields' => $this->get_fields()));
		$output .= FormFields::render($field, Context::slug($this->id), new PostMetaFormFieldData($post_id, Context::slug($this->id)));
		$output .= '</div>';
		echo($output);
	}

	public function save($post_id) {
		$field = FormFieldProperties::validate(array('type' => 'group', 'fields' => $this->get_fields()));
		FormFields::save($field, Context::slug($this->id), new PostMetaFormFieldData($post_id, Context::slug($this->id)));
	}
}
