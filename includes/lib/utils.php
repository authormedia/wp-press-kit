<?php

namespace WPPressKit;
use WPPressKit\Plugin as Context;

/*---------------------------------------------------------*/
/* Helper Functions                                        */
/*---------------------------------------------------------*/

function require_all($path) {
	foreach(scandir($path) as $file) {
		if($file[0] == '.') { continue; }
		require_once($path.DIRECTORY_SEPARATOR.$file);
	}
}

/*---------------------------------------------------------*/
/* Utility Classes                                         */
/*---------------------------------------------------------*/

class Exception extends \Exception {}

abstract class Singleton {
	static $__instances = array();

	public static function instance() {
		$class = get_called_class();
		if(!isset(self::$__instances)) { self::$__instances = array(); }
		if(!isset(self::$__instances[$class])) { self::$__instances[$class] = new $class; }
		return self::$__instances[$class];
	}

	public static function __callStatic($method, $args) {
		$instance = self::instance();
		switch (count($args)) {
			case 0:
				return $instance->$method();
			case 1:
				return $instance->$method($args[0]);
			case 2:
				return $instance->$method($args[0], $args[1]);
			case 3:
				return $instance->$method($args[0], $args[1], $args[2]);
			default:
				return call_user_func_array(array($instance, $method), $args);
		}
	}

	public static function classname() {
		return get_called_class();
	}

	public static function funcname($function) {
		return get_called_class().'::'.$function;
	}

	public static function add_action($tag, $function_to_add, $priority = 10, $accepted_args = 1) {
		add_action($tag, get_called_class().'::'.$function_to_add, $priority, $accepted_args);
	}

	public static function add_filter($tag, $function_to_add, $priority = 10, $accepted_args = 1) {
		add_filter($tag, get_called_class().'::'.$function_to_add, $priority, $accepted_args);
	}
}

abstract class PostType extends Singleton {
	protected $options;
	private $post_class = null;

	protected function __construct() {
		if(!defined(get_called_class().'::POST_TYPE')) { throw new Exception('Classes extending PostType must define the POST_TYPE constant'); }

		$this->options = array(
			'labels' => array(),
			'description' => '',
			'public' => true,
			'hierarchical' => false,
			'menu_position' => 5,
			'capability_type' => 'post',
			'supports' => array('title'),
			'has_archive' => true,
		);
	}

	protected function init() {
		static::add_action('init', 'register');
		static::add_action('save_post', 'do_save_post', 10, 3);
		static::add_filter('the_content', 'do_the_content', 100);
	}

	protected function set_option($option, $value) {
		$this->options[$option] = $value;
	}

	protected function get_option($option) {
		return $this->options[$option];
	}

	protected function generate_labels($singular_name, $plural_name) {
		$this->set_option('labels', array(
			'name' => $plural_name,
			'singular_name' => $singular_name,
			'add_new' => sprintf(__('Add New %s', Context::ID), $singular_name),
			'add_new_item' => sprintf(__('Add New %s', Context::ID), $singular_name),
			'edit_item' => sprintf(__('Edit %s', Context::ID), $singular_name),
			'new_item' => sprintf(__('New %s', Context::ID), $singular_name),
			'view_item' => sprintf(__('View %s', Context::ID), $singular_name),
			'search_items' => sprintf(__('Search %s', Context::ID), $plural_name),
			'not_found' => sprintf(__('No %s found.', Context::ID), $plural_name),
			'not_found_in_trash' => sprintf(__('No %s found in Trash.', Context::ID), $plural_name),
			'parent_item_colon' => sprintf(__('Parent %s:', Context::ID), $singular_name),
			'all_items' => sprintf(__('All %s', Context::ID), $plural_name),
			'archives' => sprintf(__('%s Archives', Context::ID), $singular_name),
			'insert_into_item' => sprintf(__('Insert into %s', Context::ID), $singular_name),
			'uploaded_to_this_item' => sprintf(__('Uploaded to this %s', Context::ID), $singular_name),
		));
	}

	protected function register() {
		register_post_type(static::POST_TYPE, $this->options);
	}

	protected function get_posts($options = null) {
		$options = wp_parse_args($options, array('posts_per_page' => -1));
		$options['post_type'] = static::POST_TYPE;
		$query = new \WP_Query($options);
		return array_map(inst_user_class_func($this->post_class()), $query->posts);
	}

	protected function is_singular() {
		return is_singular(static::POST_TYPE);
	}

	protected function is_archive() {
		return is_post_type_archive(static::POST_TYPE);
	}

	protected function get_post($post_id = null) {
		if($post_id === null) {
			if(!empty($GLOBALS['post'])) {
				$post_id = $GLOBALS['post'];
			} else {
				return false;
			}
		}
		try {
			$post_class = $this->post_class();
			return new $post_class($post_id);
		} catch(Exception $e) {
			return false;
		}
	}

	protected function post_class() {
		if($this->post_class === null) {
			$found = false;
			foreach(get_declared_classes() as $class) {
				if(is_subclass_of($class, __NAMESPACE__.'\Post')) {
					if(!defined($class.'::POST_TYPE')) { throw new Exception('Classes extending Post must define the POST_TYPE constant'); }
					if($class::POST_TYPE === static::POST_TYPE) {
						$this->post_class = $class;
						$found = true;
						break;
					}
				}
			}
			if(!$found) { $this->post_class = 'WP_Post'; }
		}
		return $this->post_class;
	}

	protected function save_post_action() {
		return Context::slug('save_post_'.static::POST_TYPE);
	}

	protected function do_save_post($post_id, $post, $update) {
		if((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || get_post_status($post_id) == 'auto-draft' || get_post_type($post_id) !== static::POST_TYPE) { return; }
		do_action(static::save_post_action(), $post_id, $post, $update);
	}

	protected function post_content_filter() {
		return Context::slug('post_content_'.static::POST_TYPE);
	}

	protected function do_the_content($content) {
		$post = $this->get_post();
		if(empty($post) or $post->post_type !== static::POST_TYPE) { return $content; }
		return apply_filters(static::post_content_filter(), $content, $post);
	}
}

abstract class Post {

	/*---------------------------------------------------------*/
	/* Variables                                               */
	/*---------------------------------------------------------*/

	public $ID;
	public $post_author = 0;
	public $post_date = '0000-00-00 00:00:00';
	public $post_date_gmt = '0000-00-00 00:00:00';
	public $post_content = '';
	public $post_title = '';
	public $post_excerpt = '';
	public $post_status = 'publish';
	public $comment_status = 'open';
	public $ping_status = 'open';
	public $post_password = '';
	public $post_name = '';
	public $to_ping = '';
	public $pinged = '';
	public $post_modified = '0000-00-00 00:00:00';
	public $post_modified_gmt = '0000-00-00 00:00:00';
	public $post_content_filtered = '';
	public $post_parent = 0;
	public $guid = '';
	public $menu_order = 0;
	public $post_type = 'post';
	public $post_mime_type = '';
	public $comment_count = 0;

	/*---------------------------------------------------------*/
	/* Public Functions                                        */
	/*---------------------------------------------------------*/

	public function __construct($post) {
		if(!defined(get_called_class().'::POST_TYPE')) { throw new Exception('Classes extending Post must define the POST_TYPE constant'); }

		if($post instanceof static or $post instanceof \WP_Post) {
			$post_data = get_object_vars($post);
		} else if(is_int($post)) {
			$post_data = wp_cache_get($post, 'posts');
			if(!$post_data or !is_object($post_data)) {
				global $wpdb;
				$post_data = $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE ID = %d LIMIT 1", $post), OBJECT);
				if(!$post_data) { throw new Exception('Invalid post ID provided to post constructor: '.strval($post)); }
				$post_data = sanitize_post($post_data, 'raw');
				wp_cache_add($post_data->ID, $post_data, 'posts');
			}
			$post_data = get_object_vars($post_data);
		} else {
			throw new Exception('Invalid argument provided to post constructor: '.strval($post));
		}

		if(!isset($post_data['ID']) or $post_data['ID'] == 0 or !isset($post_data['post_type'])) { throw new Exception('Invalid post object provided to post constructor'); }
		if($post_data['post_type'] !== static::POST_TYPE) { throw new Exception('Invalid post type "'.strval($post_data['post_type']).'", expected "'.static::POST_TYPE.'"'); }

		foreach($post_data as $key => $value) {
			$this->$key = $value;
		}
	}

	public function get_meta($key) {
		return get_post_meta($this->ID, $key, true);
	}

	public function set_meta($key, $value) {
		update_post_meta($this->ID, $key, $value);
	}
}

/*---------------------------------------------------------*/
/* Utility Functions                                       */
/*---------------------------------------------------------*/

// equivalent of call_user_func for classes
function inst_user_class($class) {
	return inst_user_class_array($class, func_get_args());
}

// equivalent of call_user_func_array for classes
function inst_user_class_array($class, $param_arr) {
	$reflection_class = new \ReflectionClass($class);
	return $reflection_class->newInstanceArgs($param_arr);
}

// returns a function that will instantiate the class
function inst_user_class_func($class) {
	return function() use ($class) {
		return inst_user_class_array($class, func_get_args());
	};
}

// gets the connanocal url for the current taxonomy/post type archive page
function get_archive_permalink() {
	if(is_tax()) {
	    $url = get_term_link(get_query_var('term'), get_query_var('taxonomy'));
	} else if(is_post_type_archive()) {
		$url = get_post_type_archive_link(get_query_var('post_type'));
	} else {
		return false;
	}

	$page = get_query_var('paged');
	if($page > 1) {
		global $wp_rewrite;
		if(!$wp_rewrite->using_permalinks()) {
			$url = add_query_arg('paged', $page, $url);
		} else {
			$url = user_trailingslashit(trailingslashit($url).trailingslashit($wp_rewrite->pagination_base).$page);
		}
		return $url;
	}

	return $url;
}

function add_ajax_action($action, $function_to_add, $nopriv = false) {
	add_action('wp_ajax_'.$action, $function_to_add);
	if($nopriv) { add_action('wp_ajax_nopriv_'.$action, $function_to_add); }
}

function post_get_json($name) {
	if(!isset($_POST[$name]) or !is_string($_POST[$name])) { return null; }
	return json_decode(str_replace('\\\\', '\\', str_replace('\\\'', '\'', str_replace('\\"', '"', $_POST[$name]))), true);
}

function post_get_int($name) {
	if(!isset($_POST[$name]) or !is_string($_POST[$name]) or !is_numeric($_POST[$name])) { return null; }
	return intval($_POST[$name]);
}

function post_get_float($name) {
	if(!isset($_POST[$name]) or !is_string($_POST[$name]) or !is_numeric($_POST[$name])) { return null; }
	return floatval($_POST[$name]);
}

function post_get_str($name) {
	if(!isset($_POST[$name]) or !is_string($_POST[$name])) { return null; }
	return wp_unslash($_POST[$name]);
}

function wp_slash_fixed($value) {
	if(is_array($value)) {
		foreach($value as $k => $v) {
			if(is_array($v)) {
				$value[$k] = wp_slash_fixed($v);
			} else if(is_string($v)) {
				$value[$k] = addslashes($v);
			}
		}
	} else if(is_string($value)) {
		$value = addslashes($value);
	}

	return $value;
}
